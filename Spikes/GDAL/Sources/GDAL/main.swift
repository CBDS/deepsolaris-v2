import CGDAL

extension Collection {
  /// Creates an UnsafeMutableBufferPointer with enough space to hold the elements of self.
  public func unsafeCopy() -> UnsafeMutableBufferPointer<Self.Element> {
    let copy = UnsafeMutableBufferPointer<Self.Element>.allocate(capacity: self.underestimatedCount)
    _ = copy.initialize(from: self)
    return copy
  }
}

extension Collection where Element == String {
  var buffer: UnsafeMutableBufferPointer<UnsafeMutablePointer<CChar>?> {
    self.map({ UnsafeMutablePointer($0.unsafeUTF8Copy().baseAddress) }).unsafeCopy()
  }

  var cslConstList: CSLConstList? {
    self.buffer.unsafeCopy().baseAddress
  }

}

extension String {
  /// Create UnsafeMutableBufferPointer holding a null-terminated UTF8 copy of the string
  public func unsafeUTF8Copy() -> UnsafeMutableBufferPointer<CChar> {
    self.utf8CString.unsafeCopy()
  }
}

func main() {
  let width: Int32 = 4000
  let height: Int32 = width

  GDALAllRegister()

  //Dataset to read
  let wmsUrl =
    "WMS:https://mapy.geoportal.gov.pl/wss/service/PZGIK/ORTO/WMS/HighResolution?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&STYLES=default&LAYERS=Raster&CRS=EPSG:2180&format=image/jpeg&BBOX=506600,357600,507400,358400&MINRESOLUTION=0.2"

  let readDataset = GDALOpen(wmsUrl, GA_ReadOnly)
  defer {
    GDALClose(readDataset)
  }

  let rasterCount = GDALGetRasterCount(readDataset)
  let numberOfBytes = Int(width * height * rasterCount)
  print("number of bands in dataset: \(rasterCount)")

  //let rasterBandData = UnsafeMutablePointer<UInt8>.allocate(capacity: numberOfBytes)
  let rasterBandData = UnsafeMutableRawPointer.allocate(byteCount: numberOfBytes, alignment: 1)
  defer {
    rasterBandData.deallocate()
  }

  let driver = GDALGetDriverByName("GTiff")

  let filename = "poznan_test.tiff"
  let options = ["COMPRESS=JPEG"].cslConstList

  let writeDataset = GDALCreate(
    driver,
    filename, width, height, rasterCount,
    GDT_Byte, options)
  defer {
    GDALClose(writeDataset)
  }

  GDALDatasetRasterIO(
    writeDataset,
    GF_Write,
    Int32(0), Int32(0),
    width, height,
    rasterBandData,
    width, height,
    GDT_Byte,
    rasterCount,
    nil,
    rasterCount, width * rasterCount, Int32(1))

  GDALFlushCache(writeDataset)
}

main()

import XCTest

import DeepSolarisKitTests

var tests = [XCTestCaseEntry]()
tests += DeepSolarisKitTests.allTests()
XCTMain(tests)

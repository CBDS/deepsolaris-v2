import Foundation
import XCTest

@testable import DeepSolarisKit

final class FilenameUtilsTests: XCTestCase {
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
    }

    func testFilenameParsedCorrectly() {
        guard let filenameParts1 = splitFilename(filename: "dop10rgbi_32_280_5652_1_nw.jp2") else {
            XCTFail("Invalid filename")
            return
        }
        XCTAssertEqual("dop10rgbi_32", filenameParts1.prefix)
        XCTAssertEqual(280, filenameParts1.x)
        XCTAssertEqual(5652, filenameParts1.y)
        XCTAssertEqual("1_nw.jp2", filenameParts1.suffix)

        guard let filenameParts2 = splitFilename(filename: "dop10rgbi_32_294_5742_1_nw.jp2") else {
            XCTFail("Invalid filename")
            return
        }
        XCTAssertEqual("dop10rgbi_32", filenameParts2.prefix)
        XCTAssertEqual(294, filenameParts2.x)
        XCTAssertEqual(5742, filenameParts2.y)
        XCTAssertEqual("1_nw.jp2", filenameParts2.suffix)

        guard let filenameParts3 = splitFilename(filename: "dop10rgbi_32_296_5738_1_nw.jp2") else {
            XCTFail("Invalid filename")
            return
        }
        XCTAssertEqual("dop10rgbi_32", filenameParts3.prefix)
        XCTAssertEqual(296, filenameParts3.x)
        XCTAssertEqual(5738, filenameParts3.y)
        XCTAssertEqual("1_nw.jp2", filenameParts3.suffix)

        guard let filenameParts4 = splitFilename(filename: "dop10rgbi_32_299_5648_1_nw.jp2") else {
            XCTFail("Invalid filename")
            return
        }
        XCTAssertEqual("dop10rgbi_32", filenameParts4.prefix)
        XCTAssertEqual(299, filenameParts4.x)
        XCTAssertEqual(5648, filenameParts4.y)
        XCTAssertEqual("1_nw.jp2", filenameParts4.suffix)

        guard let filenameParts5 = splitFilename(filename: "dop10rgbi_32_316_5730_1_nw.jp2") else {
            XCTFail("Invalid filename")
            return
        }
        XCTAssertEqual("dop10rgbi_32", filenameParts5.prefix)
        XCTAssertEqual(316, filenameParts5.x)
        XCTAssertEqual(5730, filenameParts5.y)
        XCTAssertEqual("1_nw.jp2", filenameParts5.suffix)

    }

    func testDestinationFileDerivedCorrectly() {
        XCTAssertEqual(
            "dop10rgbi_32_290000_5653900_1_nw.tiff",
            getDestinationFilename(
                sourceFilename: "dop10rgbi_32_280_5652_1_nw.jp2",
                boundingRect: Rect(
                    topLeft: Point(x: 290000, y: 5_654_000), width: 100, height: -100),
                fileExtension: "tiff"))
        XCTAssertEqual(
            "dop10rgbi_32_294000_5741900_1_nw.png",
            getDestinationFilename(
                sourceFilename: "dop10rgbi_32_294_5742_1_nw.jp2",
                boundingRect: Rect(
                    topLeft: Point(x: 294_000, y: 5_742_000), width: 100, height: -100),
                fileExtension: "png"))
        XCTAssertEqual(
            "dop10rgbi_32_296000_5737900_1_nw.jpeg",
            getDestinationFilename(
                sourceFilename: "dop10rgbi_32_296_5738_1_nw.jp2",
                boundingRect: Rect(
                    topLeft: Point(x: 296000, y: 5_738_000), width: 100, height: -100),
                fileExtension: "jpeg"))
    }

    /*func testDestinationFileDerivedCorrectlyFromFullUrl() {
        XCTAssertEqual(
            "dop10rgbi_32_290000_5653900_1_nw.tiff",
            getDestinationFilename(
                sourceFilename:
                    "file:///media/ImageData/DE/aerial%20images/HR/selections/Bonn_2019_HR/dop10rgbi_32_280_5652_1_nw.jp2",
                boundingRect: Rect(
                    topLeft: Point(x: 290000, y: 5_654_000), width: 100, height: -100),
                fileExtension: "tiff"))
        XCTAssertEqual(
            "dop10rgbi_32_294000_5741900_1_nw.png",
            getDestinationFilename(
                sourceFilename:
                    "file:///media/ImageData/DE/aerial%20images/HR/selections/Bonn_2019_HR/dop10rgbi_32_294_5742_1_nw.jp2",
                boundingRect: Rect(
                    topLeft: Point(x: 294_000, y: 5_742_000), width: 100, height: -100),
                fileExtension: "png"))
        XCTAssertEqual(
            "dop10rgbi_32_296000_5737900_1_nw.jpeg",
            getDestinationFilename(
                sourceFilename:
                    "file:///media/ImageData/DE/aerial%20images/HR/selections/Bonn_2019_HR/dop10rgbi_32_296_5738_1_nw.jp2",
                boundingRect: Rect(
                    topLeft: Point(x: 296000, y: 5_738_000), width: 100, height: -100),
                fileExtension: "jpeg"))
    }*/

    func testFilterFilesInDirectory() {
        let filteredFiles1 =
            filterFilesInDirectory(
                directory: TestUtils.testDataLocationFor(filename: "dataset_directory1/"),
                fileExtension: "png"
            )?.map { $0.lastPathComponent }.sorted() ?? []

        XCTAssertEqual(4, filteredFiles1.count)
        XCTAssertEqual("image1.png", filteredFiles1[0])
        XCTAssertEqual("image2.png", filteredFiles1[1])
        XCTAssertEqual("image3.png", filteredFiles1[2])
        XCTAssertEqual("image4.png", filteredFiles1[3])

        let filteredFiles2 =
            filterFilesInDirectory(
                directory: TestUtils.testDataLocationFor(filename: "dataset_directory2/"),
                fileExtension: "tiff"
            )?.map { $0.lastPathComponent }.sorted() ?? []
        XCTAssertEqual(4, filteredFiles2.count)
        XCTAssertEqual("image5.tiff", filteredFiles2[0])
        XCTAssertEqual("image6.tiff", filteredFiles2[1])
        XCTAssertEqual("image7.tiff", filteredFiles2[2])
        XCTAssertEqual("image8.tiff", filteredFiles2[3])

        let filteredFiles3 =
            filterFilesInDirectory(
                directory: TestUtils.testDataLocationFor(filename: "dataset_directory3/"),
                fileExtension: "jp2"
            )?.map { $0.lastPathComponent }.sorted() ?? []
        XCTAssertEqual(4, filteredFiles3.count)
        XCTAssertEqual("image1.jp2", filteredFiles3[0])
        XCTAssertEqual("image2.jp2", filteredFiles3[1])
        XCTAssertEqual("image3.jp2", filteredFiles3[2])
        XCTAssertEqual("image4.jp2", filteredFiles3[3])
    }
}

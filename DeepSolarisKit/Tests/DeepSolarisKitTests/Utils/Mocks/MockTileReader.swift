@testable import DeepSolarisKit

class MockTileReader: TileReader {
    var numberOfCalls = 0
    var readTiles: [Rect]

    init(readTiles: [Rect] = []) {
        self.readTiles = readTiles
    }

    func read(rect: Rect) -> Tile? {
        numberOfCalls += 1
        readTiles.append(rect)
        return Tile(pixelExtent: rect, mapExtent: rect, uniqueId: "file_\(readTiles.count)")
    }
}

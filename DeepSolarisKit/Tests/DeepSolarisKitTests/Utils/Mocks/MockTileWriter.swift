@testable import DeepSolarisKit

class MockTileWriter: TileWriter {
    var numberOfCalls: Int = 0
    var processedTiles: [Tile] = []

    func write(tile: Tile) 
    {
        numberOfCalls += 1
        processedTiles.append(tile)
    }
}

import Foundation

@testable import DeepSolarisKit

class MockRasterMap: RasterMap, ReadableRasterMap, WritableRasterMap {
    var mapTransform: MapTransform
    var mapExtent: Rect?
    var writtenFilenames: [URL] = []
    var writtenPixelRects: [Rect] = []
    var writtenProjectionReferences: [String] = []
    var readPixelRects: [Rect] = []
    var mapRects: [Rect] = []
    var imageData: [Data] = []
    var numberOfBands: Int = 0
    var currentImageData: Int = 0
    var projectionReference: String = ""

    init(imageData: [Data] = [], projectionReference: String = "") {
        self.mapTransform = MapTransform(worldVector: [
            0.1000000000,
            0.0000000000,
            0.0000000000,
            -0.1000000000,
            280000,
            5_653_000,
        ])
        self.mapExtent = Rect(topLeft: Point(x: 280000, y: 5_653_000), width: 1000, height: -1000)
        self.imageData = imageData
        self.projectionReference = projectionReference
    }

    init(mapTransform: MapTransform, mapExtent: Rect) {
        self.mapTransform = mapTransform
        self.mapExtent = mapExtent
    }

    func readData(pixelRect: Rect) -> Data? {
        defer {
            self.currentImageData += 1
        }
        readPixelRects.append(pixelRect)
        return self.currentImageData < imageData.count ? imageData[self.currentImageData] : nil
    }

    func writeData(
        data: Data, pixelRect: Rect, mapRect: Rect, to filename: URL, projectionReference: String
    ) {
        writtenFilenames.append(filename)
        writtenPixelRects.append(pixelRect)
        mapRects.append(mapRect)
        imageData.append(data)
        writtenProjectionReferences.append(projectionReference)
    }
}

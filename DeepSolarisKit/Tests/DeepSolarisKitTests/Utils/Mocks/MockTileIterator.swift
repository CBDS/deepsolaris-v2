@testable import DeepSolarisKit

struct MockTileIterator: TileIterator {
    var tiles: [Rect]
    var currentIndex: Int = 0
    var numberOfTiles: Int {
        return tiles.count
    }

    mutating func next() -> Rect? {
        defer {

            currentIndex += 1
        }
        return currentIndex < tiles.count ? tiles[currentIndex] : nil
    }
}

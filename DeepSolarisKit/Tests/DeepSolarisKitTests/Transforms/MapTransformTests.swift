import Foundation
import XCTest

@testable import DeepSolarisKit

final class MapTransformTests: XCTestCase {

    func testMapTransformIdentityKeepsCoordinate() {
        XCTAssertEqual(SIMD2(0.0, 0), MapTransform() * SIMD2(0.0, 0.0))
        XCTAssertEqual(SIMD2(1.0, 0), MapTransform() * SIMD2(1.0, 0.0))
        XCTAssertEqual(SIMD2(2.0, 0), MapTransform() * SIMD2(2.0, 0.0))
        XCTAssertEqual(SIMD2(3.0, 0), MapTransform() * SIMD2(3.0, 0.0))
        XCTAssertEqual(SIMD2(4.0, 0), MapTransform() * SIMD2(4.0, 0.0))
    }

    func testMapTransformXTranslatesCoordinate() {
        XCTAssertEqual(SIMD2(1.0, 0), MapTransform(translateX: 1.0) * SIMD2(0.0, 0.0))
        XCTAssertEqual(SIMD2(3.0, 0), MapTransform(translateX: 2.0) * SIMD2(1.0, 0.0))
        XCTAssertEqual(SIMD2(5.0, 0), MapTransform(translateX: 3.0) * SIMD2(2.0, 0.0))
        XCTAssertEqual(SIMD2(7.0, 0), MapTransform(translateX: 4.0) * SIMD2(3.0, 0.0))
        XCTAssertEqual(SIMD2(9.0, 0), MapTransform(translateX: 5.0) * SIMD2(4.0, 0.0))
    }

    func testMapTransformYTranslatesCoordinate() {
        XCTAssertEqual(SIMD2(0.0, 1.0), MapTransform(translateY: 1.0) * SIMD2(0.0, 0.0))
        XCTAssertEqual(SIMD2(0.0, 3.0), MapTransform(translateY: 2.0) * SIMD2(0.0, 1.0))
        XCTAssertEqual(SIMD2(0.0, 5.0), MapTransform(translateY: 3.0) * SIMD2(0.0, 2.0))
        XCTAssertEqual(SIMD2(0.0, 7.0), MapTransform(translateY: 4.0) * SIMD2(0.0, 3.0))
        XCTAssertEqual(SIMD2(0.0, 9.0), MapTransform(translateY: 5.0) * SIMD2(0.0, 4.0))
    }

    func testMapTransformYandXTranslatesCoordinate() {
        XCTAssertEqual(
            SIMD2(7.0, 7.0), MapTransform(translateX: 6.0, translateY: 1.0) * SIMD2(1.0, 6.0))
        XCTAssertEqual(
            SIMD2(9.0, 9.0), MapTransform(translateX: 7.0, translateY: 2.0) * SIMD2(2.0, 7.0))
        XCTAssertEqual(
            SIMD2(11.0, 11.0), MapTransform(translateX: 8.0, translateY: 3.0) * SIMD2(3.0, 8.0))
        XCTAssertEqual(
            SIMD2(13.0, 13.0), MapTransform(translateX: 9.0, translateY: 4.0) * SIMD2(4.0, 9.0))
        XCTAssertEqual(
            SIMD2(15.0, 15.0), MapTransform(translateX: 10.0, translateY: 5.0) * SIMD2(5.0, 10.0))

    }

    func testScaleXScalesXCoordinate() {
        XCTAssertEqual(SIMD2(1.0, 0.0), MapTransform(scaleX: 0.1) * SIMD2(10, 0.0))
        XCTAssertEqual(SIMD2(4.0, 0.0), MapTransform(scaleX: 0.2) * SIMD2(20, 0.0))
        XCTAssertEqual(SIMD2(9.0, 0.0), MapTransform(scaleX: 0.3) * SIMD2(30, 0.0))
        XCTAssertEqual(SIMD2(16.0, 0.0), MapTransform(scaleX: 0.4) * SIMD2(40, 0.0))
        XCTAssertEqual(SIMD2(25.0, 0.0), MapTransform(scaleX: 0.5) * SIMD2(50, 0.0))
    }

    func testScaleYScalesYCoordinate() {
        XCTAssertEqual(SIMD2(0.0, 1.0), MapTransform(scaleY: 0.1) * SIMD2(0.0, 10))
        XCTAssertEqual(SIMD2(0.0, 4.0), MapTransform(scaleY: 0.2) * SIMD2(0.0, 20))
        XCTAssertEqual(SIMD2(0.0, 9.0), MapTransform(scaleY: 0.3) * SIMD2(0.0, 30))
        XCTAssertEqual(SIMD2(0.0, 16.0), MapTransform(scaleY: 0.4) * SIMD2(0.0, 40))
        XCTAssertEqual(SIMD2(0.0, 25.0), MapTransform(scaleY: 0.5) * SIMD2(0.0, 50))
    }

    func testMapTransformPointTranslatesPoint() {
        XCTAssertEqual(
            Point(x: 7.0, y: 7.0),
            MapTransform(translateX: 6.0, translateY: 1.0) * Point(x: 1.0, y: 6.0))
        XCTAssertEqual(
            Point(x: 9.0, y: 9.0),
            MapTransform(translateX: 7.0, translateY: 2.0) * Point(x: 2.0, y: 7.0))
        XCTAssertEqual(
            Point(x: 11.0, y: 11.0),
            MapTransform(translateX: 8.0, translateY: 3.0) * Point(x: 3.0, y: 8.0))
        XCTAssertEqual(
            Point(x: 13.0, y: 13.0),
            MapTransform(translateX: 9.0, translateY: 4.0) * Point(x: 4.0, y: 9.0))
        XCTAssertEqual(
            Point(x: 15.0, y: 15.0),
            MapTransform(translateX: 10.0, translateY: 5.0) * Point(x: 5.0, y: 10.0))

    }

    func testMapTransformReturnsTranslatedRect() {
        XCTAssertEqual(
            Rect(topLeft: Point(x: 7.0, y: 7.0), width: 100, height: 100),
            MapTransform(translateX: 6.0, translateY: 1.0)
                * Rect(topLeft: Point(x: 1.0, y: 6.0), width: 100, height: 100))
        XCTAssertEqual(
            Rect(topLeft: Point(x: 9.0, y: 9.0), width: 200, height: 300),
            MapTransform(translateX: 7.0, translateY: 2.0)
                * Rect(topLeft: Point(x: 2.0, y: 7.0), width: 200, height: 300))
        XCTAssertEqual(
            Rect(topLeft: Point(x: 11.0, y: 11.0), width: 300, height: 300),
            MapTransform(translateX: 8.0, translateY: 3.0)
                * Rect(topLeft: Point(x: 3.0, y: 8.0), width: 300, height: 300))
        XCTAssertEqual(
            Rect(topLeft: Point(x: 13.0, y: 13.0), width: 400, height: 500),
            MapTransform(translateX: 9.0, translateY: 4.0)
                * Rect(topLeft: Point(x: 4.0, y: 9.0), width: 400, height: 500))
        XCTAssertEqual(
            Rect(topLeft: Point(x: 15.0, y: 15.0), width: 600, height: 600),
            MapTransform(translateX: 10.0, translateY: 5.0)
                * Rect(topLeft: Point(x: 5.0, y: 10.0), width: 600, height: 600))
    }

    func testFromWorldVectorReturnsMapTransform() {
        let worldVectors = [
            //dop10rgbi_32_280_5652_1_nw
            [
                0.1000000000,
                0.0000000000,
                0.0000000000,
                -0.1000000000,
                280000.0500000000,
                5652999.9500000002,
            ],
            //dop10rgbi_32_294_5742_1_nw
            [
                0.1000000000,
                0.0000000000,
                0.0000000000,
                -0.1000000000,
                294000.0500000000,
                5742999.9500000002,
            ],
            //dop10rgbi_32_296_5738_1_nw
            [
                0.1000000000,
                0.0000000000,
                0.0000000000,
                -0.1000000000,
                296000.0500000000,
                5738999.9500000002,
            ],
            //dop10rgbi_32_299_5648_1_nw
            [
                0.1000000000,
                0.0000000000,
                0.0000000000,
                -0.1000000000,
                299000.0500000000,
                5648999.9500000002,
            ],
            //dop10rgbi_32_316_5730_1_nw
            [
                0.1000000000,
                0.0000000000,
                0.0000000000,
                -0.1000000000,
                316000.0500000000,
                5730999.9500000002,
            ],
        ]

        for worldVector in worldVectors {
            let mapTransform = MapTransform(worldVector: worldVector)
            XCTAssertEqual(worldVector[0], mapTransform.transformX.x)
            XCTAssertEqual(worldVector[2], mapTransform.transformX.y)
            XCTAssertEqual(worldVector[4], mapTransform.transformX.z)
            XCTAssertEqual(worldVector[1], mapTransform.transformY.x)
            XCTAssertEqual(worldVector[3], mapTransform.transformY.y)
            XCTAssertEqual(worldVector[5], mapTransform.transformY.z)
        }
    }

    func testInitWithGDALVectorReturnsMapTransform() {
        let gdalVectors = [
            //dop10rgbi_32_280_5652_1_nw
            [
                280000.0500000000,
                0.1000000000,
                0.0000000000,
                5652999.9500000002,
                0.0000000000,
                -0.1000000000,
            ],
            //dop10rgbi_32_294_5742_1_nw
            [
                294000.0500000000,
                0.1000000000,
                0.0000000000,
                5742999.9500000002,
                0.0000000000,
                -0.1000000000,
            ],
            //dop10rgbi_32_296_5738_1_nw
            [
                296000.0500000000,
                0.1000000000,
                0.0000000000,
                5738999.9500000002,
                0.0000000000,
                -0.1000000000,
            ],
            //dop10rgbi_32_299_5648_1_nw
            [
                299000.0500000000,
                0.1000000000,
                0.0000000000,
                5648999.9500000002,
                0.0000000000,
                -0.1000000000,
            ],
            //dop10rgbi_32_316_5730_1_nw
            [
                316000.0500000000,
                0.1000000000,
                0.0000000000,
                5730999.9500000002,
                0.0000000000,
                -0.1000000000,
            ],
        ]

        for gdalVector in gdalVectors {
            let mapTransform = MapTransform(gdalVector: gdalVector)
            XCTAssertEqual(gdalVector[1], mapTransform.transformX.x)
            XCTAssertEqual(gdalVector[2], mapTransform.transformX.y)
            XCTAssertEqual(gdalVector[0], mapTransform.transformX.z)
            XCTAssertEqual(gdalVector[4], mapTransform.transformY.x)
            XCTAssertEqual(gdalVector[5], mapTransform.transformY.y)
            XCTAssertEqual(gdalVector[3], mapTransform.transformY.z)
        }
    }

    func testGdalVectorReturnedCorrectly() {
        let worldVectors = [
            //dop10rgbi_32_280_5652_1_nw
            [
                0.1000000000,
                0.0000000000,
                0.0000000000,
                -0.1000000000,
                280000.0500000000,
                5652999.9500000002,
            ],
            //dop10rgbi_32_294_5742_1_nw
            [
                0.1000000000,
                0.0000000000,
                0.0000000000,
                -0.1000000000,
                294000.0500000000,
                5742999.9500000002,
            ],
            //dop10rgbi_32_296_5738_1_nw
            [
                0.1000000000,
                0.0000000000,
                0.0000000000,
                -0.1000000000,
                296000.0500000000,
                5738999.9500000002,
            ],
            //dop10rgbi_32_299_5648_1_nw
            [
                0.1000000000,
                0.0000000000,
                0.0000000000,
                -0.1000000000,
                299000.0500000000,
                5648999.9500000002,
            ],
            //dop10rgbi_32_316_5730_1_nw
            [
                0.1000000000,
                0.0000000000,
                0.0000000000,
                -0.1000000000,
                316000.0500000000,
                5730999.9500000002,
            ],
        ]

        for worldVector in worldVectors {
            let mapTransform = MapTransform(worldVector: worldVector)
            XCTAssertEqual(
                [
                    mapTransform.transformX.z,
                    mapTransform.transformX.x,
                    mapTransform.transformX.y,
                    mapTransform.transformY.z,
                    mapTransform.transformY.x,
                    mapTransform.transformY.y,
                ], mapTransform.gdalVector)
        }
    }

    func testDeterminantCalculatedCorrectly() {
        // Determinant of 3x3 matrix:
        // https://www.mathsisfun.com/algebra/matrix-determinant.html
        // [[a b c]
        // [d e f]
        // [g h i]]
        // det = a(ei − fh) − b(di − fg) + c(dh − eg)
        // MapTransform
        // [[scaleX, skewX, translateX]
        // [skewY, scaleY, translateY],
        // [0 , 0, 1]]
        // det = scaleX * (scaleY - translateY * 0) - skewX * (skewY - translateY * 0) =
        // det = scaleX * scaleY - skewX * skewY = a * e - b * d = 2x2 det

        XCTAssertEqual(
            4.0,
            MapTransform(
                scaleX: 1.0, scaleY: 4.0, skewX: 1.0, skewY: 0.0
            ).determinant)

        XCTAssertEqual(
            2.0,
            MapTransform(
                scaleX: 1.0, scaleY: 4.0, skewX: 1.0, skewY: 2.0
            ).determinant)

        XCTAssertEqual(
            -1.0,
            MapTransform(
                scaleX: 2.0, scaleY: 4.0, skewX: 3.0, skewY: 3.0
            ).determinant)

        XCTAssertEqual(
            3.0,
            MapTransform(
                scaleX: 3.0, scaleY: 5.0, skewX: 2.0, skewY: 6.0
            ).determinant)

        XCTAssertEqual(
            0.0,
            MapTransform(
                scaleX: 4.0, scaleY: 3.0, skewX: 2.0, skewY: 6.0
            ).determinant)
    }

    func testIsDegenerateReturnedCorrectly() {
        //IsDegenerate: det == 0
        XCTAssertFalse(
            MapTransform(
                scaleX: 1.0, scaleY: 4.0, skewX: 1.0, skewY: 0.0
            ).isDegenerate)

        XCTAssertFalse(
            MapTransform(
                scaleX: 1.0, scaleY: 4.0, skewX: 1.0, skewY: 2.0
            ).isDegenerate)

        XCTAssertFalse(
            MapTransform(
                scaleX: 2.0, scaleY: 4.0, skewX: 3.0, skewY: 3.0
            ).isDegenerate)

        XCTAssertTrue(
            MapTransform(
                scaleX: 4.0, scaleY: 3.0, skewX: 2.0, skewY: 6.0
            ).isDegenerate)

        XCTAssertTrue(
            MapTransform(
                scaleX: 2.0, scaleY: 8.0, skewX: 4.0, skewY: 4.0
            ).isDegenerate)

        XCTAssertTrue(
            MapTransform(
                scaleX: 6.0, scaleY: 3.0, skewX: 9.0, skewY: 2.0
            ).isDegenerate)
    }

    func testInvertFailsWhenDeterminantDegenerate() {
        XCTAssertNil(
            MapTransform(
                scaleX: 4.0, scaleY: 3.0, skewX: 2.0, skewY: 6.0
            ).inverse)

        XCTAssertNil(
            MapTransform(
                scaleX: 2.0, scaleY: 8.0, skewX: 4.0, skewY: 4.0
            ).inverse)

        XCTAssertNil(
            MapTransform(
                scaleX: 6.0, scaleY: 3.0, skewX: 9.0, skewY: 2.0
            ).inverse)
    }

    func testTransformCalculatedFromPixelAndMapExtent() {
        //TODO add tests for skew
        let mapExtents = [
            Rect(topLeft: Point(x: 280000.0, y: 5_654_000), width: 1000, height: -1000),
            Rect(topLeft: Point(x: 290000.0, y: 5_554_000), width: 4000, height: -4000),
            Rect(topLeft: Point(x: 190700.0, y: 327600), width: 1000, height: -1000),
            Rect(topLeft: Point(x: 172700.0, y: 338400), width: 1000, height: -1000),
            Rect(topLeft: Point(x: 351835.0, y: 509270), width: 5000, height: -5000),
            Rect(topLeft: Point(x: 263493.0, y: 553607), width: 4000, height: -4000),
        ]
        let pixelExtents = [
            Rect(topLeft: Point(x: 0, y: 0), width: 10000, height: 10000),
            Rect(topLeft: Point(x: 0, y: 0), width: 16000, height: 16000),
            Rect(topLeft: Point(x: 0, y: 0), width: 10000, height: 10000),
            Rect(topLeft: Point(x: 0, y: 0), width: 4000, height: 4000),
            Rect(topLeft: Point(x: 0, y: 0), width: 10000, height: 10000),
            Rect(topLeft: Point(x: 0, y: 0), width: 20000, height: 20000),
        ]

        XCTAssertEqual(
            MapTransform(worldVector: [
                0.1,
                0.0,
                0.0,
                -0.1,
                280000.0,
                5654000.0,
            ]), MapTransform(mapExtent: mapExtents[0], pixelExtent: pixelExtents[0]))
        XCTAssertEqual(
            MapTransform(worldVector: [
                0.25,
                0.0,
                0.0,
                -0.25,
                290000.0,
                5554000.0,
            ]), MapTransform(mapExtent: mapExtents[1], pixelExtent: pixelExtents[1]))
        XCTAssertEqual(
            MapTransform(worldVector: [
                0.1,
                0.0,
                0.0,
                -0.1,
                190700.0,
                327600.0,
            ]), MapTransform(mapExtent: mapExtents[2], pixelExtent: pixelExtents[2]))
        XCTAssertEqual(
            MapTransform(worldVector: [
                0.25,
                0.0,
                0.0,
                -0.25,
                172700.0,
                338400.0,
            ]), MapTransform(mapExtent: mapExtents[3], pixelExtent: pixelExtents[3]))
        XCTAssertEqual(
            MapTransform(worldVector: [
                0.5,
                0.0,
                0.0,
                -0.5,
                351835.0,
                509270.0,
            ]), MapTransform(mapExtent: mapExtents[4], pixelExtent: pixelExtents[4]))
        XCTAssertEqual(
            MapTransform(worldVector: [
                0.2,
                0.0,
                0.0,
                -0.2,
                263493.0,
                553607.0,
            ]), MapTransform(mapExtent: mapExtents[5], pixelExtent: pixelExtents[5]))
    }

    func testEquatableReturnsEquality() {
        XCTAssertEqual(
            MapTransform(worldVector: [0, 1, 2, 3, 4, 5]),
            MapTransform(worldVector: [0, 1, 2, 3, 4, 5]))
        XCTAssertEqual(
            MapTransform(worldVector: [1, 1, 2, 2, 3, 3]),
            MapTransform(worldVector: [1, 1, 2, 2, 3, 3]))
        XCTAssertEqual(
            MapTransform(worldVector: [5, 6, 7, 8, 9, 10]),
            MapTransform(worldVector: [5, 6, 7, 8, 9, 10]))
        XCTAssertNotEqual(
            MapTransform(worldVector: [1, 1, 2, 3, 4, 5]),
            MapTransform(worldVector: [0, 1, 2, 3, 4, 5]))
        XCTAssertNotEqual(
            MapTransform(worldVector: [5, 5, 7, 8, 9, 10]),
            MapTransform(worldVector: [5, 6, 7, 8, 9, 10]))
    }
}

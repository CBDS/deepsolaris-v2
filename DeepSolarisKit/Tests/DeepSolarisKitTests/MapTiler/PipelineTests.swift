import XCTest

@testable import DeepSolarisKit

class PipelineTests: XCTestCase
{
    var originalRects: [Rect]!

    class MockTileIterator: TileIterator
    {
        var currentRectIndex = 0
        var rects: [Rect] = [Rect]()
        var numberOfTiles: Int {
            return rects.count
        }

        init(rects: [Rect])
        {
            self.rects = rects
        }

        func next() -> Rect? 
        { 
            if (currentRectIndex >= rects.count)
            {
                return nil
            }

            let rect = rects[currentRectIndex]
            currentRectIndex += 1
            return rect
        }
    }

    class TileCreator: TileReader
    {
        var currentRectIndex = 0

        init()
        {            
        }

        func read(rect: Rect) -> Tile? {
            let tile = Tile(pixelExtent: rect, mapExtent: rect, uniqueId: "\(currentRectIndex)")
            currentRectIndex += 1
            return tile
        }
    }

    struct TileMoveProcessor: TileProcessor
    {
        func process(tile: Tile) -> Tile?
        {            
            return Tile(pixelExtent: tile.pixelExtent.move(deltaX: 10, deltaY: 10), 
                    mapExtent: tile.mapExtent.move(deltaX: 10, deltaY: 10), 
                    uniqueId: tile.uniqueId)
        }
    }

    struct TileCopyProcessor: TileProcessor
    {
        func process(tile: Tile) -> Tile?
        {
            return tile
        }
    }

    class TileSink : TileWriter
    {
        var tiles: [Tile] = [Tile]()

        func write(tile: Tile) 
        {
            tiles.append(tile)
        }
    }

    override func setUpWithError() throws 
    {
        self.originalRects = [
            Rect(topLeft: Point(x: 0.0, y: 0.0), bottomRight: Point(x: 10.0, y: 10.0)),
            Rect(topLeft: Point(x: 1.0, y: 5.0), bottomRight: Point(x: 20.0, y: 15.0)),
            Rect(topLeft: Point(x: 2.0, y: 10.0), bottomRight: Point(x: 30.0, y: 20.0)),
            Rect(topLeft: Point(x: 3.0, y: 15.0), bottomRight: Point(x: 40.0, y: 25.0)),
            Rect(topLeft: Point(x: 4.0, y: 20.0), bottomRight: Point(x: 50.0, y: 30.0)),
        ]
    }
   
    func testPipelineResultBuilderBuildsPipeline() async
    {
        let tileCreator = TileCreator()
        let tileIterator = MockTileIterator(rects: originalRects) 
        let tileSink = TileSink()

        let pipeline = Pipeline(source: tileCreator, sink: tileSink, tileIterator: tileIterator) {
            TileMoveProcessor()
        }

        await pipeline.run()

        if tileSink.tiles.count == 0 
        {
            XCTFail("No tiles were returned by pipeline 1")
            return
        }

        XCTAssertEqual(originalRects[0].move(deltaX: 10, deltaY: 10), tileSink.tiles[0].pixelExtent)
        XCTAssertEqual(originalRects[1].move(deltaX: 10, deltaY: 10), tileSink.tiles[1].pixelExtent)
        XCTAssertEqual(originalRects[2].move(deltaX: 10, deltaY: 10), tileSink.tiles[2].pixelExtent)
        XCTAssertEqual(originalRects[3].move(deltaX: 10, deltaY: 10), tileSink.tiles[3].pixelExtent)
        XCTAssertEqual(originalRects[4].move(deltaX: 10, deltaY: 10), tileSink.tiles[4].pixelExtent)

        let tileIterator2 = MockTileIterator(rects: originalRects)  
        let tileSink2 = TileSink()

        let pipeline2 = Pipeline(source: tileCreator, sink: tileSink2, tileIterator: tileIterator2) {
            TileMoveProcessor()
            TileMoveProcessor()
        }

        await pipeline2.run()

        if tileSink.tiles.count == 0 
        {
            XCTFail("No tiles were returned by pipeline 1")
            return
        }

        XCTAssertEqual(originalRects[0].move(deltaX: 10, deltaY: 10).move(deltaX: 10, deltaY: 10), tileSink2.tiles[0].pixelExtent)
        XCTAssertEqual(originalRects[1].move(deltaX: 10, deltaY: 10).move(deltaX: 10, deltaY: 10), tileSink2.tiles[1].pixelExtent)
        XCTAssertEqual(originalRects[2].move(deltaX: 10, deltaY: 10).move(deltaX: 10, deltaY: 10), tileSink2.tiles[2].pixelExtent)
        XCTAssertEqual(originalRects[3].move(deltaX: 10, deltaY: 10).move(deltaX: 10, deltaY: 10), tileSink2.tiles[3].pixelExtent)
        XCTAssertEqual(originalRects[4].move(deltaX: 10, deltaY: 10).move(deltaX: 10, deltaY: 10), tileSink2.tiles[4].pixelExtent)
    }

    actor ProgressCallback: PipelineProgressCallback
    {
        var events: [PipelineProgressEvent] = [PipelineProgressEvent]()

        func progress(_ event: PipelineProgressEvent) 
        {
            events.append(event)
        }        
    }

    func testPipelineReportsProgressCorrectly() async
    {
        let progressCallback = ProgressCallback()
        let tileCreator = TileCreator()
        let tileIterator = MockTileIterator(rects: originalRects) 
        let tileSink = TileSink()

        let pipeline = Pipeline(source: tileCreator, 
            sink: tileSink, 
            tileIterator: tileIterator,
            callback: progressCallback) 
        {
           TileCopyProcessor()
        }
        await pipeline.run()

        let events = await progressCallback.events
        var tileIndex = 0
        var numberOfEventsHandled = 0
        for (index, event) in events.enumerated()
        {
            switch(event)
            {
                case .pipelineStarted:
                    XCTAssertTrue(index == 0)
                    numberOfEventsHandled += 1

                case let .pipelineReadTile(tile ):
                    XCTAssertEqual(self.originalRects[tileIndex], tile.pixelExtent)
                    numberOfEventsHandled += 1

                case let .pipelineTileProcessed( _, tile):
                    XCTAssertEqual(self.originalRects[tileIndex], tile.pixelExtent)
                    numberOfEventsHandled += 1
                    
                case let .pipelineWroteTile(tile):
                    XCTAssertEqual(self.originalRects[tileIndex], tile.pixelExtent)
                    tileIndex += 1
                    numberOfEventsHandled += 1

                case .pipelineEnded:
                    XCTAssertEqual(events.count, index+1)
                    numberOfEventsHandled += 1
            }  
        }
        XCTAssertEqual(17, numberOfEventsHandled)
    }
}
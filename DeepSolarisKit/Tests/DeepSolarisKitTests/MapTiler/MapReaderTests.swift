import XCTest

@testable import DeepSolarisKit

final class MapReaderTests: XCTestCase {
    let tileData = [
        Data(count: 1),
        Data(count: 2),
        Data(count: 3),
        Data(count: 4),
        Data(count: 5),
    ]

    let tileRects = [
        Rect(topLeft: Point(x: 0.0, y: 0.0), width: 100, height: 200),
        Rect(topLeft: Point(x: 1000.0, y: 1000.0), width: 200, height: 300),
        Rect(topLeft: Point(x: 2000.0, y: 2000.0), width: 300, height: 400),
        Rect(topLeft: Point(x: 3000.0, y: 3000.0), width: 400, height: 500),
        Rect(topLeft: Point(x: 4000.0, y: 4000.0), width: 500, height: 600),
    ]

    var mapReader: MapReader?

    override func setUp() {
        self.mapReader = MapReader(map: MockRasterMap(imageData: tileData))
    }

    func testReadReturnsTileWithPassedPixelExtent() {
        XCTAssertEqual(tileRects[0], mapReader?.read(rect: tileRects[0])?.pixelExtent)
        XCTAssertEqual(tileRects[1], mapReader?.read(rect: tileRects[1])?.pixelExtent)
        XCTAssertEqual(tileRects[2], mapReader?.read(rect: tileRects[2])?.pixelExtent)
        XCTAssertEqual(tileRects[3], mapReader?.read(rect: tileRects[3])?.pixelExtent)
        XCTAssertEqual(tileRects[4], mapReader?.read(rect: tileRects[4])?.pixelExtent)
    }

    func testReadReturnsMapExtentForTile() {
        XCTAssertEqual(
            Rect(topLeft: Point(x: 280000, y: 5_653_000), width: 10, height: -20),
            mapReader?.read(rect: tileRects[0])?.mapExtent)
        XCTAssertEqual(
            Rect(topLeft: Point(x: 280100, y: 5_652_900), width: 20, height: -30),
            mapReader?.read(rect: tileRects[1])?.mapExtent)
        XCTAssertEqual(
            Rect(topLeft: Point(x: 280200, y: 5_652_800), width: 30, height: -40),
            mapReader?.read(rect: tileRects[2])?.mapExtent)
        XCTAssertEqual(
            Rect(topLeft: Point(x: 280300, y: 5_652_700), width: 40, height: -50),
            mapReader?.read(rect: tileRects[3])?.mapExtent)
        XCTAssertEqual(
            Rect(topLeft: Point(x: 280400, y: 5_652_600), width: 50, height: -60),
            mapReader?.read(rect: tileRects[4])?.mapExtent)
    }

    func testUniqueIdDerivedFromTileMapExtentBottomLeft() {
        XCTAssertEqual(
            "E280000N5652980",
            mapReader?.read(rect: tileRects[0])?.uniqueId)
        XCTAssertEqual(
            "E280100N5652870",
            mapReader?.read(rect: tileRects[1])?.uniqueId)
        XCTAssertEqual(
            "E280200N5652760",
            mapReader?.read(rect: tileRects[2])?.uniqueId)
        XCTAssertEqual(
            "E280300N5652650",
            mapReader?.read(rect: tileRects[3])?.uniqueId)
        XCTAssertEqual(
            "E280400N5652540",
            mapReader?.read(rect: tileRects[4])?.uniqueId)
    }

    func testReadReturnsProjectionReferenceForTile() {
        XCTAssertEqual(
            "projection1",
            MapReader(map: MockRasterMap(imageData: tileData, projectionReference: "projection1"))
                .read(
                    rect: tileRects[0])?.projectionReference)
        XCTAssertEqual(
            "projection2",
            MapReader(map: MockRasterMap(imageData: tileData, projectionReference: "projection2"))
                .read(
                    rect: tileRects[0])?.projectionReference)
        XCTAssertEqual(
            "projection3",
            MapReader(map: MockRasterMap(imageData: tileData, projectionReference: "projection3"))
                .read(
                    rect: tileRects[0])?.projectionReference)
    }

    func testMapReaderCallsReadOnReadableRasterMap() {
        let mockRasterMap = MockRasterMap(imageData: tileData)
        let mapReader1 = MapReader(map: mockRasterMap)
        XCTAssertEqual(tileRects[0], mapReader1.read(rect: tileRects[0])?.pixelExtent)
        XCTAssertEqual(tileRects[1], mapReader1.read(rect: tileRects[1])?.pixelExtent)
        XCTAssertEqual(tileRects[2], mapReader1.read(rect: tileRects[2])?.pixelExtent)
        XCTAssertEqual(tileRects[3], mapReader1.read(rect: tileRects[3])?.pixelExtent)
        XCTAssertEqual(tileRects[4], mapReader1.read(rect: tileRects[4])?.pixelExtent)
        XCTAssertEqual(tileRects, mockRasterMap.readPixelRects)
    }

}

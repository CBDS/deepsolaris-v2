import XCTest

@testable import DeepSolarisKit

final class MapWriterTests: XCTestCase {
    let mockRasterMap = MockRasterMap()
    let dummyRect = Rect(topLeft: Point(x: 0.0, y: 0.0), width: 10, height: 10)

    func testAddsDestinationPathAndExtensionForFilename() {
        let mapWriter1 = MapWriter(
            destinationUrl: TestUtils.testDataLocationFor(filename: "destination_directory1/"),
            fileExtension: "jpg"
        ) {
            return self.mockRasterMap
        }

        mapWriter1.write(
            tile: Tile(pixelExtent: dummyRect, mapExtent: dummyRect, uniqueId: "E280000N5732000"))
        XCTAssertEqual(
            TestUtils.testDataLocationFor(filename: "destination_directory1/E280000N5732000.jpg"),
            mockRasterMap.writtenFilenames.count > 0 ? mockRasterMap.writtenFilenames[0] : nil)
        mapWriter1.write(
            tile: Tile(pixelExtent: dummyRect, mapExtent: dummyRect, uniqueId: "E280100N5732500"))
        XCTAssertEqual(
            TestUtils.testDataLocationFor(filename: "destination_directory1/E280100N5732500.jpg"),
            mockRasterMap.writtenFilenames.count > 0 ? mockRasterMap.writtenFilenames[1] : nil)

        let mapWriter2 = MapWriter(
            destinationUrl: TestUtils.testDataLocationFor(filename: "destination_directory2/"),
            fileExtension: "png"
        ) {
            return self.mockRasterMap
        }

        mapWriter2.write(
            tile: Tile(pixelExtent: dummyRect, mapExtent: dummyRect, uniqueId: "E290000N5832000"))
        XCTAssertEqual(
            TestUtils.testDataLocationFor(filename: "destination_directory2/E290000N5832000.png"),
            mockRasterMap.writtenFilenames.count > 0 ? mockRasterMap.writtenFilenames[2] : nil)
        mapWriter2.write(
            tile: Tile(pixelExtent: dummyRect, mapExtent: dummyRect, uniqueId: "E290100N5832500"))
        XCTAssertEqual(
            TestUtils.testDataLocationFor(filename: "destination_directory2/E290100N5832500.png"),
            mockRasterMap.writtenFilenames.count > 0 ? mockRasterMap.writtenFilenames[3] : nil)
    }

    func testPassesTilePixelExtentToWriteData() {
        let mapWriter = MapWriter(
            destinationUrl: TestUtils.testDataLocationFor(filename: "destination_directory1/"),
            fileExtension: "jpg"
        ) {
            return self.mockRasterMap
        }

        let pixelExtent = [
            Rect(topLeft: Point(x: 0, y: 0), width: 100, height: -100),
            Rect(topLeft: Point(x: 100, y: 200), width: 100, height: -100),
            Rect(topLeft: Point(x: 200, y: 300), width: 100, height: -100),
            Rect(topLeft: Point(x: 300, y: 400), width: 100, height: -100),
            Rect(topLeft: Point(x: 400, y: 500), width: 100, height: -100),
        ]

        mapWriter.write(
            tile: Tile(
                pixelExtent: pixelExtent[0], mapExtent: dummyRect, uniqueId: "E280000N5732000")
        )
        mapWriter.write(
            tile: Tile(
                pixelExtent: pixelExtent[1], mapExtent: dummyRect, uniqueId: "E280100N5732100")
        )
        mapWriter.write(
            tile: Tile(
                pixelExtent: pixelExtent[2], mapExtent: dummyRect, uniqueId: "E290000N5733000")
        )
        mapWriter.write(
            tile: Tile(
                pixelExtent: pixelExtent[3], mapExtent: dummyRect, uniqueId: "E290100N5733100")
        )
        mapWriter.write(
            tile: Tile(
                pixelExtent: pixelExtent[4], mapExtent: dummyRect, uniqueId: "E300000N5734000")
        )

        XCTAssertEqual(pixelExtent, mockRasterMap.writtenPixelRects)
    }

    func testPassesTileMapExtentToWriteData() {
        let mapWriter = MapWriter(
            destinationUrl: TestUtils.testDataLocationFor(filename: "destination_directory1/"),
            fileExtension: "jpg"
        ) {
            return self.mockRasterMap
        }

        let mapExtent = [
            Rect(topLeft: Point(x: 280000, y: 5_731_900), width: 100, height: -100),
            Rect(topLeft: Point(x: 280100, y: 5_732_000), width: 100, height: -100),
            Rect(topLeft: Point(x: 290000, y: 5_732_900), width: 100, height: -100),
            Rect(topLeft: Point(x: 290100, y: 5_733_000), width: 100, height: -100),
            Rect(topLeft: Point(x: 300000, y: 5_733_900), width: 100, height: -100),
        ]

        mapWriter.write(
            tile: Tile(pixelExtent: dummyRect, mapExtent: mapExtent[0], uniqueId: "E280000N5732000")
        )
        mapWriter.write(
            tile: Tile(pixelExtent: dummyRect, mapExtent: mapExtent[1], uniqueId: "E280100N5732100")
        )
        mapWriter.write(
            tile: Tile(pixelExtent: dummyRect, mapExtent: mapExtent[2], uniqueId: "E290000N5733000")
        )
        mapWriter.write(
            tile: Tile(pixelExtent: dummyRect, mapExtent: mapExtent[3], uniqueId: "E290100N5733100")
        )
        mapWriter.write(
            tile: Tile(pixelExtent: dummyRect, mapExtent: mapExtent[4], uniqueId: "E300000N5734000")
        )

        XCTAssertEqual(mapExtent, mockRasterMap.mapRects)
    }

    func testPassesTileDataToWriteData() {
        let mapWriter = MapWriter(
            destinationUrl: TestUtils.testDataLocationFor(filename: "destination_directory1/"),
            fileExtension: "jpg"
        ) {
            return self.mockRasterMap
        }

        let tileData = [
            Data(count: 1),
            Data(count: 2),
            Data(count: 3),
            Data(count: 4),
            Data(count: 5),
        ]

        mapWriter.write(
            tile: Tile(
                data: tileData[0], pixelExtent: dummyRect, mapExtent: dummyRect,
                uniqueId: "E280000N5732000")
        )
        mapWriter.write(
            tile: Tile(
                data: tileData[1], pixelExtent: dummyRect, mapExtent: dummyRect,
                uniqueId: "E280100N5732100")
        )
        mapWriter.write(
            tile: Tile(
                data: tileData[2], pixelExtent: dummyRect, mapExtent: dummyRect,
                uniqueId: "E290000N5733000")
        )
        mapWriter.write(
            tile: Tile(
                data: tileData[3], pixelExtent: dummyRect, mapExtent: dummyRect,
                uniqueId: "E290100N5733100")
        )
        mapWriter.write(
            tile: Tile(
                data: tileData[4], pixelExtent: dummyRect, mapExtent: dummyRect,
                uniqueId: "E300000N5734000")
        )

        XCTAssertEqual(tileData, mockRasterMap.imageData)
    }

    func testPassesProjectionReferenceToWriteData() {
        let mapWriter = MapWriter(
            destinationUrl: TestUtils.testDataLocationFor(filename: "destination_directory1/"),
            fileExtension: "jpg"
        ) {
            return self.mockRasterMap
        }

        let tileData = [
            Data(count: 1),
            Data(count: 2),
            Data(count: 3),
            Data(count: 4),
            Data(count: 5),
        ]

        let projectionReferences = [
            "projection1",
            "projection2",
            "projection3",
            "projection4",
            "projection5",
        ]

        mapWriter.write(
            tile: Tile(
                data: tileData[0], pixelExtent: dummyRect, mapExtent: dummyRect,
                uniqueId: "E280000N5732000", projectionReference: projectionReferences[0])
        )
        mapWriter.write(
            tile: Tile(
                data: tileData[1], pixelExtent: dummyRect, mapExtent: dummyRect,
                uniqueId: "E280100N5732100", projectionReference: projectionReferences[1])
        )
        mapWriter.write(
            tile: Tile(
                data: tileData[2], pixelExtent: dummyRect, mapExtent: dummyRect,
                uniqueId: "E290000N5733000", projectionReference: projectionReferences[2])
        )
        mapWriter.write(
            tile: Tile(
                data: tileData[3], pixelExtent: dummyRect, mapExtent: dummyRect,
                uniqueId: "E290100N5733100", projectionReference: projectionReferences[3])
        )
        mapWriter.write(
            tile: Tile(
                data: tileData[4], pixelExtent: dummyRect, mapExtent: dummyRect,
                uniqueId: "E300000N5734000", projectionReference: projectionReferences[4])
        )

        XCTAssertEqual(projectionReferences, mockRasterMap.writtenProjectionReferences)
    }
}

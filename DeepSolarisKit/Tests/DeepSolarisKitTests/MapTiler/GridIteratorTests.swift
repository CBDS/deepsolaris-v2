import XCTest

@testable import DeepSolarisKit

final class GridIteratorTests: XCTestCase {

    let mockRasterMap = MockRasterMap(
        mapTransform: MapTransform(worldVector: [
            0.1000000000,
            0.0000000000,
            0.0000000000,
            -0.1000000000,
            280000,
            5_653_000,
        ]), mapExtent: Rect(topLeft: Point(x: 280000, y: 5_653_000), width: 1000, height: -1000)
    )

    func testGridIteratorReturnsTilesForMap() {
        var gridIterator = GridIterator(map: mockRasterMap, strideX: 2000, strideY: 2000)
        // Row 0
        XCTAssertEqual(
            Rect(topLeft: Point(x: 0.0, y: 0.0), width: 2000, height: 2000), gridIterator?.next())
        XCTAssertEqual(
            Rect(topLeft: Point(x: 2000.0, y: 0.0), width: 2000, height: 2000), gridIterator?.next()
        )
        XCTAssertEqual(
            Rect(topLeft: Point(x: 4000.0, y: 0.0), width: 2000, height: 2000), gridIterator?.next()
        )
        XCTAssertEqual(
            Rect(topLeft: Point(x: 6000.0, y: 0.0), width: 2000, height: 2000), gridIterator?.next()
        )
        XCTAssertEqual(
            Rect(topLeft: Point(x: 8000.0, y: 0.0), width: 2000, height: 2000), gridIterator?.next()
        )

        // Row 1
        XCTAssertEqual(
            Rect(topLeft: Point(x: 0.0, y: 2000.0), width: 2000, height: 2000), gridIterator?.next()
        )
        XCTAssertEqual(
            Rect(topLeft: Point(x: 2000.0, y: 2000.0), width: 2000, height: 2000),
            gridIterator?.next())
        XCTAssertEqual(
            Rect(topLeft: Point(x: 4000.0, y: 2000.0), width: 2000, height: 2000),
            gridIterator?.next())
        XCTAssertEqual(
            Rect(topLeft: Point(x: 6000.0, y: 2000.0), width: 2000, height: 2000),
            gridIterator?.next())
        XCTAssertEqual(
            Rect(topLeft: Point(x: 8000.0, y: 2000.0), width: 2000, height: 2000),
            gridIterator?.next())

        // Row 2
        XCTAssertEqual(
            Rect(topLeft: Point(x: 0.0, y: 4000.0), width: 2000, height: 2000), gridIterator?.next()
        )
        XCTAssertEqual(
            Rect(topLeft: Point(x: 2000.0, y: 4000.0), width: 2000, height: 2000),
            gridIterator?.next())
        XCTAssertEqual(
            Rect(topLeft: Point(x: 4000.0, y: 4000.0), width: 2000, height: 2000),
            gridIterator?.next())
        XCTAssertEqual(
            Rect(topLeft: Point(x: 6000.0, y: 4000.0), width: 2000, height: 2000),
            gridIterator?.next())
        XCTAssertEqual(
            Rect(topLeft: Point(x: 8000.0, y: 4000.0), width: 2000, height: 2000),
            gridIterator?.next())

        // Row 3
        XCTAssertEqual(
            Rect(topLeft: Point(x: 0.0, y: 6000.0), width: 2000, height: 2000), gridIterator?.next()
        )
        XCTAssertEqual(
            Rect(topLeft: Point(x: 2000.0, y: 6000.0), width: 2000, height: 2000),
            gridIterator?.next())
        XCTAssertEqual(
            Rect(topLeft: Point(x: 4000.0, y: 6000.0), width: 2000, height: 2000),
            gridIterator?.next())
        XCTAssertEqual(
            Rect(topLeft: Point(x: 6000.0, y: 6000.0), width: 2000, height: 2000),
            gridIterator?.next())
        XCTAssertEqual(
            Rect(topLeft: Point(x: 8000.0, y: 6000.0), width: 2000, height: 2000),
            gridIterator?.next())

        // Row 4
        XCTAssertEqual(
            Rect(topLeft: Point(x: 0.0, y: 8000.0), width: 2000, height: 2000), gridIterator?.next()
        )
        XCTAssertEqual(
            Rect(topLeft: Point(x: 2000.0, y: 8000.0), width: 2000, height: 2000),
            gridIterator?.next())
        XCTAssertEqual(
            Rect(topLeft: Point(x: 4000.0, y: 8000.0), width: 2000, height: 2000),
            gridIterator?.next())
        XCTAssertEqual(
            Rect(topLeft: Point(x: 6000.0, y: 8000.0), width: 2000, height: 2000),
            gridIterator?.next())
        XCTAssertEqual(
            Rect(topLeft: Point(x: 8000.0, y: 8000.0), width: 2000, height: 2000),
            gridIterator?.next())
    }
}

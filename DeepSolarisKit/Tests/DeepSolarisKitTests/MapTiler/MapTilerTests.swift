import XCTest

@testable import DeepSolarisKit

final class MapTilerTests: XCTestCase {

    let tileIterator = MockTileIterator(tiles: [
        Rect(topLeft: Point(x: 0.0, y: 0.0), width: 100, height: 100),
        Rect(topLeft: Point(x: 1.0, y: 4.0), width: 200, height: 600),
        Rect(topLeft: Point(x: 2.0, y: 5.0), width: 300, height: 700),
        Rect(topLeft: Point(x: 3.0, y: 6.0), width: 400, height: 800),
        Rect(topLeft: Point(x: 4.0, y: 7.0), width: 500, height: 900),
    ])

    func testReaderCalledForEachTile() {
        let tileReader = MockTileReader()
        let mapTiler = MapTiler(
            tileIterator: self.tileIterator, tileReader: tileReader,
            tileProcessor: MockTileWriter())
        mapTiler.run()
        XCTAssertEqual(self.tileIterator.tiles, tileReader.readTiles)
    }

    func testTileProcessorCalledForEachTileRead() {
        let tileProcessor = MockTileWriter()
        let mapTiler = MapTiler(
            tileIterator: self.tileIterator, tileReader: MockTileReader(),
            tileProcessor: tileProcessor)
        mapTiler.run()

        XCTAssertEqual(self.tileIterator.tiles.count, tileProcessor.processedTiles.count)
        for (index, rect) in tileIterator.tiles.enumerated() {
            XCTAssertEqual(rect, tileProcessor.processedTiles[index].pixelExtent)
            XCTAssertEqual("file_\(index+1)", tileProcessor.processedTiles[index].uniqueId)
        }
    }

    func testCallsProgressFunctionForEachTile() {
        var tileIndices: [Int] = []
        var totalNumbers: [Int] = []

        let tileProcessor = MockTileWriter()
        let mapTiler = MapTiler(
            tileIterator: self.tileIterator, tileReader: MockTileReader(),
            tileProcessor: tileProcessor)
        mapTiler.run { (tileIndex, totalNumberOfTiles) in
            tileIndices.append(tileIndex)
            totalNumbers.append(totalNumberOfTiles)
        }

        XCTAssertEqual(5, tileIndices.count)
        XCTAssertEqual(5, totalNumbers.count)
        XCTAssertEqual([Int](1...5), tileIndices)
        XCTAssertEqual([Int](repeating: 5, count: 5), totalNumbers)
    }

    func testCallsProgressFunctionForNumberOfStepsEachTile() {
        var tileIndices: [Int] = []
        var totalNumbers: [Int] = []

        let tileProcessor = MockTileWriter()
        let mapTiler = MapTiler(
            tileIterator: self.tileIterator, tileReader: MockTileReader(),
            tileProcessor: tileProcessor, progressSteps: 2)
        mapTiler.run { (tileIndex, totalNumberOfTiles) in
            tileIndices.append(tileIndex)
            totalNumbers.append(totalNumberOfTiles)
        }

        XCTAssertEqual(2, tileIndices.count)
        XCTAssertEqual(2, totalNumbers.count)
        XCTAssertEqual([2, 4], tileIndices)
        XCTAssertEqual([Int](repeating: 5, count: 2), totalNumbers)
    }
}

/*import XCTest

@testable import DeepSolarisKit

final class DirectoryTilerTests: XCTestCase {

    func testDirectoryTilerReturnsBoundingRectsForFilesInDirectory() {
        let tileReader = MockTileReader()
        var directoryTiler = DirectoryTiler(
            at: TestUtils.testDataLocationFor(filename: "nrw_jp2_dataset"), fileExtension: "jp2",
            with: tileReader)

        XCTAssertEqual(
            Rect(topLeft: Point(x: 280000, y: 5_653_000), width: 1000, height: -1000),
            directoryTiler
                .next() ?? nil)
        XCTAssertEqual(
            Rect(topLeft: Point(x: 294000, y: 5_743_000), width: 1000, height: -1000),
            directoryTiler.next() ?? nil)
        XCTAssertEqual(
            Rect(topLeft: Point(x: 296000, y: 5_739_000), width: 1000, height: -1000),
            directoryTiler.next() ?? nil)
        XCTAssertEqual(
            Rect(topLeft: Point(x: 299000, y: 5_649_000), width: 1000, height: -1000),
            directoryTiler.next() ?? nil)
        XCTAssertEqual(
            Rect(topLeft: Point(x: 316000, y: 5_731_000), width: 1000, height: -1000),
            directoryTiler.next() ?? nil)
    }

    func testDirectoryTilerCallsTileReaderForRect() {
        let tileReader = MockTileReader(readTiles: [
            Rect(topLeft: Point(x: 316000, y: 5_731_000), width: 1000, height: -1000),
            Rect(topLeft: Point(x: 299000, y: 5_649_000), width: 1000, height: -1000),
            Rect(topLeft: Point(x: 296000, y: 5_739_000), width: 1000, height: -1000),
            Rect(topLeft: Point(x: 294000, y: 5_743_000), width: 1000, height: -1000),
            Rect(topLeft: Point(x: 280000, y: 5_653_000), width: 1000, height: -1000),
        ])

        let currentIndex = 0
        let directoryTiler = DirectoryTiler(
            at: TestUtils.testDataLocationFor(filename: "nrw_jp2_dataset"), fileExtension: "jp2"
        ) {
            mapUrl in tileReader
        }

        XCTAssertEqual(5, tileReader.numberOfCalls)
        for (index, rect) in directoryTiler.enumerated() {
            XCTAssertEqual(tileReader.readTiles[index], rect)
        }
    }
}*/

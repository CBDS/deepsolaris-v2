import XCTest

@testable import DeepSolarisKit

final class RectTests: XCTestCase {
    func testBottomRightCalculatedCorrectly() {
        XCTAssertEqual(
            Point(x: 200, y: 1000),
            Rect(topLeft: Point(x: 100, y: 600), width: 100, height: 400).bottomRight)
        XCTAssertEqual(
            Point(x: 400, y: 1200),
            Rect(topLeft: Point(x: 200, y: 700), width: 200, height: 500).bottomRight)
        XCTAssertEqual(
            Point(x: 600, y: 1400),
            Rect(topLeft: Point(x: 300, y: 800), width: 300, height: 600).bottomRight)
        XCTAssertEqual(
            Point(x: 300, y: 600),
            Rect(topLeft: Point(x: 400, y: 900), width: -100, height: -300).bottomRight)
        XCTAssertEqual(
            Point(x: 300, y: 600),
            Rect(topLeft: Point(x: 500, y: 1000), width: -200, height: -400).bottomRight)
    }

    func testBottomLeftCalculatedCorrectly() {
        XCTAssertEqual(
            Point(x: 100, y: 1000),
            Rect(topLeft: Point(x: 100, y: 600), width: 100, height: 400).bottomLeft)
        XCTAssertEqual(
            Point(x: 200, y: 1200),
            Rect(topLeft: Point(x: 200, y: 700), width: 200, height: 500).bottomLeft)
        XCTAssertEqual(
            Point(x: 300, y: 1400),
            Rect(topLeft: Point(x: 300, y: 800), width: 300, height: 600).bottomLeft)
        XCTAssertEqual(
            Point(x: 400, y: 600),
            Rect(topLeft: Point(x: 400, y: 900), width: -100, height: -300).bottomLeft)
        XCTAssertEqual(
            Point(x: 500, y: 600),
            Rect(topLeft: Point(x: 500, y: 1000), width: -200, height: -400).bottomLeft)
    }

    func testRectReturnsEqualityCorrectly() {
        XCTAssertTrue(
            Rect(topLeft: Point(x: 1.0, y: 2.0), width: 100, height: 200)
                == Rect(topLeft: Point(x: 1.0, y: 2.0), width: 100, height: 200))
        XCTAssertTrue(
            Rect(topLeft: Point(x: 2.0, y: 2.0), width: 200, height: 300)
                == Rect(topLeft: Point(x: 2.0, y: 2.0), width: 200, height: 300))
        XCTAssertTrue(
            Rect(topLeft: Point(x: 3.0, y: 2.0), width: 300, height: 400)
                == Rect(topLeft: Point(x: 3.0, y: 2.0), width: 300, height: 400))
        XCTAssertTrue(
            Rect(topLeft: Point(x: 4.0, y: 2.0), width: 400, height: 500)
                == Rect(topLeft: Point(x: 4.0, y: 2.0), width: 400, height: 500))
        XCTAssertTrue(
            Rect(topLeft: Point(x: 5.0, y: 2.0), width: 500, height: 600)
                == Rect(topLeft: Point(x: 5.0, y: 2.0), width: 500, height: 600))
    }

    func testRectFromPointsInitializedCorrectly() {
        XCTAssertEqual(
            Rect(topLeft: Point(x: 10.0, y: 20.0), width: 100, height: 200),
            Rect(topLeft: Point(x: 10.0, y: 20.0), bottomRight: Point(x: 110.0, y: 220.0)))
        XCTAssertEqual(
            Rect(topLeft: Point(x: 20.0, y: 30.0), width: 200, height: 300),
            Rect(topLeft: Point(x: 20.0, y: 30.0), bottomRight: Point(x: 220.0, y: 330.0)))
        XCTAssertEqual(
            Rect(topLeft: Point(x: 30.0, y: 40.0), width: 300, height: 400),
            Rect(topLeft: Point(x: 30.0, y: 40.0), bottomRight: Point(x: 330.0, y: 440.0)))
        XCTAssertEqual(
            Rect(topLeft: Point(x: 40.0, y: 50.0), width: 400, height: 500),
            Rect(topLeft: Point(x: 40.0, y: 50.0), bottomRight: Point(x: 440.0, y: 550.0)))
        XCTAssertEqual(
            Rect(topLeft: Point(x: 50.0, y: 60.0), width: 500, height: 600),
            Rect(topLeft: Point(x: 50.0, y: 60.0), bottomRight: Point(x: 550.0, y: 660.0)))
    }
}

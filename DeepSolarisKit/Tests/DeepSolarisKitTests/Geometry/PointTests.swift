import XCTest

@testable import DeepSolarisKit

final class PointTests: XCTestCase {
    func testPointEqualTrueIfPointsSame() {
        XCTAssertTrue(Point(x: 1.0, y: 2.0) == Point(x: 1.0, y: 2.0))
        XCTAssertTrue(Point(x: 2.0, y: 3.0) == Point(x: 2.0, y: 3.0))
        XCTAssertTrue(Point(x: 3.0, y: 4.0) == Point(x: 3.0, y: 4.0))
        XCTAssertTrue(Point(x: 4.0, y: 5.0) == Point(x: 4.0, y: 5.0))
        XCTAssertTrue(Point(x: 5.0, y: 6.0) == Point(x: 5.0, y: 6.0))
    }

    func testPointToSimd2dConvertedCorrectly() {
        XCTAssertEqual(SIMD2(1.0, 6.0), Point(x: 1.0, y: 6.0).vector2)
        XCTAssertEqual(SIMD2(2.0, 7.0), Point(x: 2.0, y: 7.0).vector2)
        XCTAssertEqual(SIMD2(3.0, 8.0), Point(x: 3.0, y: 8.0).vector2)
        XCTAssertEqual(SIMD2(4.0, 9.0), Point(x: 4.0, y: 9.0).vector2)
        XCTAssertEqual(SIMD2(5.0, 10.0), Point(x: 5.0, y: 10.0).vector2)
    }

    func testSimd2ToPointConvertedCorrectly() {
        XCTAssertEqual(Point(x: 1.0, y: 6.0), Point(vector2: SIMD2(1.0, 6.0)))
        XCTAssertEqual(Point(x: 2.0, y: 7.0), Point(vector2: SIMD2(2.0, 7.0)))
        XCTAssertEqual(Point(x: 3.0, y: 8.0), Point(vector2: SIMD2(3.0, 8.0)))
        XCTAssertEqual(Point(x: 4.0, y: 9.0), Point(vector2: SIMD2(4.0, 9.0)))
        XCTAssertEqual(Point(x: 5.0, y: 10.0), Point(vector2: SIMD2(5.0, 10.0)))
    }

    func testPoint2DToSimd3dConvertedCorrectly() {
        XCTAssertEqual(SIMD3(1.0, 6.0, 1.0), Point(x: 1.0, y: 6.0).vector3)
        XCTAssertEqual(SIMD3(2.0, 7.0, 1.0), Point(x: 2.0, y: 7.0).vector3)
        XCTAssertEqual(SIMD3(3.0, 8.0, 1.0), Point(x: 3.0, y: 8.0).vector3)
        XCTAssertEqual(SIMD3(4.0, 9.0, 1.0), Point(x: 4.0, y: 9.0).vector3)
        XCTAssertEqual(SIMD3(5.0, 10.0, 1.0), Point(x: 5.0, y: 10.0).vector3)
    }

    func testPoint3DToSimd3dConvertedCorrectly() {
        XCTAssertEqual(SIMD3(1.0, 6.0, 11.0), Point(x: 1.0, y: 6.0, z: 11.0).vector3)
        XCTAssertEqual(SIMD3(2.0, 7.0, 12.0), Point(x: 2.0, y: 7.0, z: 12.0).vector3)
        XCTAssertEqual(SIMD3(3.0, 8.0, 13.0), Point(x: 3.0, y: 8.0, z: 13.0).vector3)
        XCTAssertEqual(SIMD3(4.0, 9.0, 14.0), Point(x: 4.0, y: 9.0, z: 14.0).vector3)
        XCTAssertEqual(SIMD3(5.0, 10.0, 15.0), Point(x: 5.0, y: 10.0, z: 15.0).vector3)
    }

    func testSimd3ToPointConvertedCorrectly() {
        XCTAssertEqual(Point(x: 1.0, y: 6.0, z: 11.0), Point(vector3: SIMD3(1.0, 6.0, 11.0)))
        XCTAssertEqual(Point(x: 2.0, y: 7.0, z: 12.0), Point(vector3: SIMD3(2.0, 7.0, 12.0)))
        XCTAssertEqual(Point(x: 3.0, y: 8.0, z: 13.0), Point(vector3: SIMD3(3.0, 8.0, 13.0)))
        XCTAssertEqual(Point(x: 4.0, y: 9.0, z: 14.0), Point(vector3: SIMD3(4.0, 9.0, 14.0)))
        XCTAssertEqual(Point(x: 5.0, y: 10.0, z: 15.0), Point(vector3: SIMD3(5.0, 10.0, 15.0)))
    }

    func testPoints2DAddedCorrectly() {
        XCTAssertEqual(Point(x: 1.0, y: 2.0), Point(x: -1.0, y: 0.0) + Point(x: 2.0, y: 2.0))
        XCTAssertEqual(Point(x: 2.0, y: 3.0), Point(x: 1.0, y: 0.0) + Point(x: 1.0, y: 3.0))
        XCTAssertEqual(Point(x: 3.0, y: 4.0), Point(x: -1.0, y: 2.0) + Point(x: 4.0, y: 2.0))
        XCTAssertEqual(Point(x: 4.0, y: 5.0), Point(x: 5.0, y: 4.0) + Point(x: -1.0, y: 1.0))
        XCTAssertEqual(Point(x: 5.0, y: 6.0), Point(x: 3.0, y: 2.0) + Point(x: 2.0, y: 4.0))
    }

    func testPoints3DAddedCorrectly() {
        XCTAssertEqual(
            Point(x: 1.0, y: 2.0, z: 10),
            Point(x: -1.0, y: 0.0, z: 6.0) + Point(x: 2.0, y: 2.0, z: 4.0))
        XCTAssertEqual(
            Point(x: 2.0, y: 3.0, z: 11),
            Point(x: 1.0, y: 0.0, z: 10.0) + Point(x: 1.0, y: 3.0, z: 1.0))
        XCTAssertEqual(
            Point(x: 3.0, y: 4.0, z: 12),
            Point(x: -1.0, y: 2.0, z: 7.0) + Point(x: 4.0, y: 2.0, z: 5.0))
        XCTAssertEqual(
            Point(x: 4.0, y: 5.0, z: 13),
            Point(x: 5.0, y: 4.0, z: 3.0) + Point(x: -1.0, y: 1.0, z: 10.0))
        XCTAssertEqual(
            Point(x: 5.0, y: 6.0, z: 14),
            Point(x: 3.0, y: 2.0, z: 7.0) + Point(x: 2.0, y: 4.0, z: 7.0))
    }

    static var allTests = [
        ("testPointToSimd2dConvertedCorrectly", testPointToSimd2dConvertedCorrectly),
        ("testPoint2DToSimd3dConvertedCorrectly", testPoint2DToSimd3dConvertedCorrectly),
        ("testPoint3DToSimd3dConvertedCorrectly", testPoint3DToSimd3dConvertedCorrectly),
    ]
}

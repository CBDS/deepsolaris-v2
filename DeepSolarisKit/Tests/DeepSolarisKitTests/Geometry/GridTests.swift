import Foundation
import XCTest

@testable import DeepSolarisKit

final class GridTests: XCTestCase {
    func testGridNumberOfRowsReturnedCorrectly() {
        XCTAssertEqual(2, Grid(width: 10, height: 20, strideX: 5, strideY: 10).numberOfRows)
        XCTAssertEqual(3, Grid(width: 10, height: 30, strideX: 10, strideY: 10).numberOfRows)
        XCTAssertEqual(4, Grid(width: 100, height: 200, strideX: 25, strideY: 50).numberOfRows)
        XCTAssertEqual(4, Grid(width: 100, height: 200, strideX: 21, strideY: 50).numberOfRows)
        XCTAssertEqual(5, Grid(width: 100, height: 200, strideX: 21, strideY: 41).numberOfRows)
    }

    func testGridNumberOfColumnsReturnedCorrectly() {
        XCTAssertEqual(2, Grid(width: 10, height: 20, strideX: 5, strideY: 10).numberOfColumns)
        XCTAssertEqual(1, Grid(width: 10, height: 30, strideX: 10, strideY: 10).numberOfColumns)
        XCTAssertEqual(4, Grid(width: 100, height: 200, strideX: 25, strideY: 50).numberOfColumns)
        XCTAssertEqual(5, Grid(width: 100, height: 200, strideX: 21, strideY: 50).numberOfColumns)
        XCTAssertEqual(5, Grid(width: 100, height: 200, strideX: 21, strideY: 41).numberOfColumns)
    }

    func testGridNumberOfCellsReturnedCorrectly() {
        XCTAssertEqual(4, Grid(width: 10, height: 20, strideX: 5, strideY: 10).numberOfCells)
        XCTAssertEqual(3, Grid(width: 10, height: 30, strideX: 10, strideY: 10).numberOfCells)
        XCTAssertEqual(16, Grid(width: 100, height: 200, strideX: 25, strideY: 50).numberOfCells)
        XCTAssertEqual(20, Grid(width: 100, height: 200, strideX: 21, strideY: 50).numberOfCells)
        XCTAssertEqual(25, Grid(width: 100, height: 200, strideX: 21, strideY: 41).numberOfCells)
    }

    func testGridCellsCreatedCorrectly() {
        var grid1 = Grid(width: 10, height: 20, strideX: 5, strideY: 10)
        XCTAssertEqual(Rect(topLeft: Point(x: 0, y: 0), width: 5, height: 10), grid1.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 5, y: 0), width: 5, height: 10), grid1.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 0, y: 10), width: 5, height: 10), grid1.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 5, y: 10), width: 5, height: 10), grid1.next())

        var grid2 = Grid(width: 10, height: 30, strideX: 10, strideY: 10)
        XCTAssertEqual(Rect(topLeft: Point(x: 0, y: 0), width: 10, height: 10), grid2.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 0, y: 10), width: 10, height: 10), grid2.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 0, y: 20), width: 10, height: 10), grid2.next())

        var grid3 = Grid(width: 100, height: 200, strideX: 25, strideY: 50)
        XCTAssertEqual(Rect(topLeft: Point(x: 0, y: 0), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 25, y: 0), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 50, y: 0), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 75, y: 0), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 0, y: 50), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 25, y: 50), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 50, y: 50), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 75, y: 50), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 0, y: 100), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 25, y: 100), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 50, y: 100), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 75, y: 100), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 0, y: 150), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 25, y: 150), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 50, y: 150), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 75, y: 150), width: 25, height: 50), grid3.next())
    }

    func testLastGridCellsCreatedCorrectly() {
        var grid1 = Grid(width: 10, height: 20, strideX: 6, strideY: 10)
        XCTAssertEqual(Rect(topLeft: Point(x: 0, y: 0), width: 6, height: 10), grid1.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 6, y: 0), width: 4, height: 10), grid1.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 0, y: 10), width: 6, height: 10), grid1.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 6, y: 10), width: 4, height: 10), grid1.next())

        var grid2 = Grid(width: 10, height: 30, strideX: 10, strideY: 11)
        XCTAssertEqual(Rect(topLeft: Point(x: 0, y: 0), width: 10, height: 11), grid2.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 0, y: 11), width: 10, height: 11), grid2.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 0, y: 22), width: 10, height: 8), grid2.next())

        var grid3 = Grid(width: 100, height: 200, strideX: 27, strideY: 51)
        XCTAssertEqual(Rect(topLeft: Point(x: 0, y: 0), width: 27, height: 51), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 27, y: 0), width: 27, height: 51), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 54, y: 0), width: 27, height: 51), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 81, y: 0), width: 19, height: 51), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 0, y: 51), width: 27, height: 51), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 27, y: 51), width: 27, height: 51), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 54, y: 51), width: 27, height: 51), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 81, y: 51), width: 19, height: 51), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 0, y: 102), width: 27, height: 51), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 27, y: 102), width: 27, height: 51), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 54, y: 102), width: 27, height: 51), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 81, y: 102), width: 19, height: 51), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 0, y: 153), width: 27, height: 47), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 27, y: 153), width: 27, height: 47), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 54, y: 153), width: 27, height: 47), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 81, y: 153), width: 19, height: 47), grid3.next())
    }

    func testGridNumberOfRowsWithStartYReturnedCorrectly() {
        XCTAssertEqual(
            2, Grid(startY: 10, width: 10, height: 20, strideX: 5, strideY: 10).numberOfRows)
        XCTAssertEqual(
            3, Grid(startY: 10, width: 10, height: 30, strideX: 10, strideY: 10).numberOfRows)
        XCTAssertEqual(
            4, Grid(startY: 50, width: 100, height: 200, strideX: 25, strideY: 50).numberOfRows)
        XCTAssertEqual(
            4, Grid(startY: 2, width: 100, height: 200, strideX: 21, strideY: 50).numberOfRows)
        XCTAssertEqual(
            5, Grid(startY: 40, width: 100, height: 200, strideX: 21, strideY: 41).numberOfRows)
    }

    func testGridNumberOfColumnsWithStartXReturnedCorrectly() {
        XCTAssertEqual(
            2, Grid(startX: 5, width: 10, height: 20, strideX: 5, strideY: 10).numberOfColumns)
        XCTAssertEqual(
            1, Grid(startX: 2, width: 10, height: 30, strideX: 10, strideY: 10).numberOfColumns)
        XCTAssertEqual(
            4, Grid(startX: 25, width: 100, height: 200, strideX: 25, strideY: 50).numberOfColumns)
        XCTAssertEqual(
            5, Grid(startX: 21, width: 100, height: 200, strideX: 21, strideY: 50).numberOfColumns)
        XCTAssertEqual(
            5, Grid(startX: 22, width: 100, height: 200, strideX: 21, strideY: 41).numberOfColumns)
    }

    func testGridNumberOfCellsWithStartPositionReturnedCorrectly() {
        XCTAssertEqual(
            4,
            Grid(startX: 5, startY: 10, width: 10, height: 20, strideX: 5, strideY: 10)
                .numberOfCells)
        XCTAssertEqual(
            3,
            Grid(startX: 2, startY: 10, width: 10, height: 30, strideX: 10, strideY: 10)
                .numberOfCells)
        XCTAssertEqual(
            16,
            Grid(startX: 25, startY: 50, width: 100, height: 200, strideX: 25, strideY: 50)
                .numberOfCells)
        XCTAssertEqual(
            20,
            Grid(startX: 21, startY: 2, width: 100, height: 200, strideX: 21, strideY: 50)
                .numberOfCells)
        XCTAssertEqual(
            25,
            Grid(startX: 22, startY: 40, width: 100, height: 200, strideX: 21, strideY: 41)
                .numberOfCells)
    }

    func testGridCellsWithStartPositionCreatedCorrectly() {
        var grid1 = Grid(startX: 2, startY: 3, width: 10, height: 20, strideX: 5, strideY: 10)
        XCTAssertEqual(Rect(topLeft: Point(x: 2, y: 3), width: 5, height: 10), grid1.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 7, y: 3), width: 5, height: 10), grid1.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 2, y: 13), width: 5, height: 10), grid1.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 7, y: 13), width: 5, height: 10), grid1.next())

        var grid2 = Grid(startX: 10, width: 10, height: 30, strideX: 10, strideY: 10)
        XCTAssertEqual(Rect(topLeft: Point(x: 10, y: 0), width: 10, height: 10), grid2.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 10, y: 10), width: 10, height: 10), grid2.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 10, y: 20), width: 10, height: 10), grid2.next())

        var grid3 = Grid(startX: 10, startY: 20, width: 100, height: 200, strideX: 25, strideY: 50)
        XCTAssertEqual(Rect(topLeft: Point(x: 10, y: 20), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 35, y: 20), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 60, y: 20), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 85, y: 20), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 10, y: 70), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 35, y: 70), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 60, y: 70), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 85, y: 70), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 10, y: 120), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 35, y: 120), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 60, y: 120), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 85, y: 120), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 10, y: 170), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 35, y: 170), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 60, y: 170), width: 25, height: 50), grid3.next())
        XCTAssertEqual(Rect(topLeft: Point(x: 85, y: 170), width: 25, height: 50), grid3.next())
    }

    func testGridFromRectCreatedCorrectly() {
        XCTAssertEqual(
            Rect(topLeft: Point(x: 0, y: 10), width: 100, height: 120),
            Grid(
                rect: Rect(topLeft: Point(x: 0, y: 10), width: 100, height: 120), strideX: 10,
                strideY: 10
            ).boundingRect)
        XCTAssertEqual(
            Rect(topLeft: Point(x: 10, y: 20), width: 200, height: 130),
            Grid(
                rect: Rect(topLeft: Point(x: 10, y: 20), width: 200, height: 130), strideX: 10,
                strideY: 10
            ).boundingRect)
        XCTAssertEqual(
            Rect(topLeft: Point(x: 20, y: 30), width: 300, height: 140),
            Grid(
                rect: Rect(topLeft: Point(x: 20, y: 30), width: 300, height: 140), strideX: 10,
                strideY: 10
            ).boundingRect)
        XCTAssertEqual(
            Rect(topLeft: Point(x: 30, y: 40), width: 400, height: 150),
            Grid(
                rect: Rect(topLeft: Point(x: 30, y: 40), width: 400, height: 150), strideX: 10,
                strideY: 10
            ).boundingRect)
        XCTAssertEqual(
            Rect(topLeft: Point(x: 40, y: 50), width: 500, height: 160),
            Grid(
                rect: Rect(topLeft: Point(x: 40, y: 50), width: 500, height: 160), strideX: 10,
                strideY: 10
            ).boundingRect)
    }

}

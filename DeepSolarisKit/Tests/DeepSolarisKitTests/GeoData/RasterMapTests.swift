import XCTest

@testable import DeepSolarisKit

final class RasterMapTests: XCTestCase {

    func testPixelExtentCalculatedFromMapExtent() {
        let worldVectors = [
            //dop10rgbi_32_280_5652_1_nw
            Rect(topLeft: Point(x: 280000, y: 5_653_000), width: 1000, height: -1000):
                [
                    0.1000000000,
                    0.0000000000,
                    0.0000000000,
                    -0.1000000000,
                    280000,
                    5_653_000,
                ],
            //dop10rgbi_32_294_5742_1_nw
            Rect(topLeft: Point(x: 294000, y: 5_743_000), width: 1000, height: -1000):
                [
                    0.1000000000,
                    0.0000000000,
                    0.0000000000,
                    -0.1000000000,
                    294000,
                    5_743_000,
                ],
            //dop10rgbi_32_296_5738_1_nw
            Rect(topLeft: Point(x: 296000, y: 5_739_000), width: 1000, height: -1000):
                [
                    0.1000000000,
                    0.0000000000,
                    0.0000000000,
                    -0.1000000000,
                    296000,
                    5_739_000,
                ],
            //dop10rgbi_32_299_5648_1_nw
            Rect(topLeft: Point(x: 299000, y: 5_649_000), width: 1000, height: -1000):
                [
                    0.1000000000,
                    0.0000000000,
                    0.0000000000,
                    -0.1000000000,
                    299000,
                    5_649_000,
                ],
            Rect(topLeft: Point(x: 316000, y: 5_731_000), width: 1000, height: -1000):
                //dop10rgbi_32_316_5730_1_nw
                [
                    0.1000000000,
                    0.0000000000,
                    0.0000000000,
                    -0.1000000000,
                    316000,
                    5_731_000,
                ],
        ]
        for (mapRect, worldVector) in worldVectors {
            let mapTransform = MapTransform(worldVector: worldVector)
            let stubbedMap = MockRasterMap(mapTransform: mapTransform, mapExtent: mapRect)

            guard let pixelExtent = stubbedMap.pixelExtent else {
                XCTFail("Pixel Extent not available")
                return
            }
            XCTAssertEqual(0, pixelExtent.topLeft.x, accuracy: 1e-1)
            XCTAssertEqual(0, pixelExtent.topLeft.y, accuracy: 1e-1)
            XCTAssertEqual(10000, pixelExtent.width)
            XCTAssertEqual(10000, pixelExtent.height)
        }
    }
}

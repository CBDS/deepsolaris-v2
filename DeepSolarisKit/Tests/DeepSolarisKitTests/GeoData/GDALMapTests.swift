import XCTest

@testable import DeepSolarisKit

final class GDALMapTests: XCTestCase {

    func testMapTransformReadCorrectly() {
        XCTAssertEqual(
            MapTransform(translateX: 360000, translateY: 5_614_000, scaleX: 0.1, scaleY: -0.1),
            ReadableGDALMap(
                mapUrl: TestUtils.testDataLocationFor(filename: "nrw_tiffs/E360000N5613980.tiff")
            )?.mapTransform)
        XCTAssertEqual(
            MapTransform(translateX: 360140, translateY: 5_614_000, scaleX: 0.1, scaleY: -0.1),
            ReadableGDALMap(
                mapUrl: TestUtils.testDataLocationFor(filename: "nrw_tiffs/E360140N5613980.tiff")
            )?.mapTransform)
        XCTAssertEqual(
            MapTransform(translateX: 360440, translateY: 5_614_000, scaleX: 0.1, scaleY: -0.1),
            ReadableGDALMap(
                mapUrl: TestUtils.testDataLocationFor(filename: "nrw_tiffs/E360440N5613980.tiff")
            )?.mapTransform)
        XCTAssertEqual(
            MapTransform(translateX: 360560, translateY: 5_614_000, scaleX: 0.1, scaleY: -0.1),
            ReadableGDALMap(
                mapUrl: TestUtils.testDataLocationFor(filename: "nrw_tiffs/E360560N5613980.tiff")
            )?.mapTransform)
        XCTAssertEqual(
            MapTransform(translateX: 360640, translateY: 5_614_000, scaleX: 0.1, scaleY: -0.1),
            ReadableGDALMap(
                mapUrl: TestUtils.testDataLocationFor(filename: "nrw_tiffs/E360640N5613980.tiff")
            )?.mapTransform)
    }

    func testPixelExtentReadCorrectly() {
        XCTAssertEqual(
            Rect(topLeft: Point(x: 0, y: 0), width: 200, height: 200),
            ReadableGDALMap(
                mapUrl: TestUtils.testDataLocationFor(filename: "nrw_tiffs/E360000N5613980.tiff")
            )?.pixelExtent)
        XCTAssertEqual(
            Rect(topLeft: Point(x: 0, y: 0), width: 200, height: 200),
            ReadableGDALMap(
                mapUrl: TestUtils.testDataLocationFor(filename: "nrw_tiffs/E360140N5613980.tiff")
            )?.pixelExtent)
        XCTAssertEqual(
            Rect(topLeft: Point(x: 0, y: 0), width: 200, height: 200),
            ReadableGDALMap(
                mapUrl: TestUtils.testDataLocationFor(filename: "nrw_tiffs/E360440N5613980.tiff")
            )?.pixelExtent)
        XCTAssertEqual(
            Rect(topLeft: Point(x: 0, y: 0), width: 200, height: 200),
            ReadableGDALMap(
                mapUrl: TestUtils.testDataLocationFor(filename: "nrw_tiffs/E360560N5613980.tiff")
            )?.pixelExtent)
        XCTAssertEqual(
            Rect(topLeft: Point(x: 0, y: 0), width: 200, height: 200),
            ReadableGDALMap(
                mapUrl: TestUtils.testDataLocationFor(filename: "nrw_tiffs/E360640N5613980.tiff")
            )?.pixelExtent)
    }

    func testMapExtentReadCorrectly() {
        XCTAssertEqual(
            Rect(topLeft: Point(x: 360000, y: 5_614_000), width: 20, height: -20),
            ReadableGDALMap(
                mapUrl: TestUtils.testDataLocationFor(filename: "nrw_tiffs/E360000N5613980.tiff")
            )?.mapExtent)
        XCTAssertEqual(
            Rect(topLeft: Point(x: 360140, y: 5_614_000), width: 20, height: -20),
            ReadableGDALMap(
                mapUrl: TestUtils.testDataLocationFor(filename: "nrw_tiffs/E360140N5613980.tiff")
            )?.mapExtent)
        XCTAssertEqual(
            Rect(topLeft: Point(x: 360440, y: 5_614_000), width: 20, height: -20),
            ReadableGDALMap(
                mapUrl: TestUtils.testDataLocationFor(filename: "nrw_tiffs/E360440N5613980.tiff")
            )?.mapExtent)
        XCTAssertEqual(
            Rect(topLeft: Point(x: 360560, y: 5_614_000), width: 20, height: -20),
            ReadableGDALMap(
                mapUrl: TestUtils.testDataLocationFor(filename: "nrw_tiffs/E360560N5613980.tiff")
            )?.mapExtent)
        XCTAssertEqual(
            Rect(topLeft: Point(x: 360640, y: 5_614_000), width: 20, height: -20),
            ReadableGDALMap(
                mapUrl: TestUtils.testDataLocationFor(filename: "nrw_tiffs/E360640N5613980.tiff")
            )?.mapExtent)
    }

    func testReadsNumberOfBandsCorrectly() {
        XCTAssertEqual(
            4,
            ReadableGDALMap(
                mapUrl: TestUtils.testDataLocationFor(filename: "nrw_tiffs/E360000N5613980.tiff")
            )?.numberOfBands)
        XCTAssertEqual(
            4,
            ReadableGDALMap(
                mapUrl: TestUtils.testDataLocationFor(filename: "nrw_tiffs/E360140N5613980.tiff")
            )?.numberOfBands)
        XCTAssertEqual(
            4,
            ReadableGDALMap(
                mapUrl: TestUtils.testDataLocationFor(filename: "nrw_tiffs/E360440N5613980.tiff")
            )?.numberOfBands)
        XCTAssertEqual(
            4,
            ReadableGDALMap(
                mapUrl: TestUtils.testDataLocationFor(filename: "nrw_tiffs/E360560N5613980.tiff")
            )?.numberOfBands)
        XCTAssertEqual(
            4,
            ReadableGDALMap(
                mapUrl: TestUtils.testDataLocationFor(filename: "nrw_tiffs/E360640N5613980.tiff")
            )?.numberOfBands)
    }

    func testReadsProjectionStringFromFile() {
        let expectedProjection =
            "PROJCS[\"ETRS89 / UTM zone 32N\",GEOGCS[\"ETRS89\",DATUM[\"European_Terrestrial_Reference_System_1989\",SPHEROID[\"GRS 1980\",6378137,298.257222101,AUTHORITY[\"EPSG\",\"7019\"]],AUTHORITY[\"EPSG\",\"6258\"]],PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]],UNIT[\"degree\",0.0174532925199433,AUTHORITY[\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"4258\"]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"latitude_of_origin\",0],PARAMETER[\"central_meridian\",9],PARAMETER[\"scale_factor\",0.9996],PARAMETER[\"false_easting\",500000],PARAMETER[\"false_northing\",0],UNIT[\"metre\",1,AUTHORITY[\"EPSG\",\"9001\"]],AXIS[\"Easting\",EAST],AXIS[\"Northing\",NORTH],AUTHORITY[\"EPSG\",\"25832\"]]"

        XCTAssertEqual(
            expectedProjection,
            ReadableGDALMap(
                mapUrl: TestUtils.testDataLocationFor(filename: "nrw_tiffs/E360000N5613980.tiff")
            )?.projectionReference)
        XCTAssertEqual(
            expectedProjection,
            ReadableGDALMap(
                mapUrl: TestUtils.testDataLocationFor(filename: "nrw_tiffs/E360140N5613980.tiff")
            )?.projectionReference)
        XCTAssertEqual(
            expectedProjection,
            ReadableGDALMap(
                mapUrl: TestUtils.testDataLocationFor(filename: "nrw_tiffs/E360440N5613980.tiff")
            )?.projectionReference)
        XCTAssertEqual(
            expectedProjection,
            ReadableGDALMap(
                mapUrl: TestUtils.testDataLocationFor(filename: "nrw_tiffs/E360560N5613980.tiff")
            )?.projectionReference)
        XCTAssertEqual(
            expectedProjection,
            ReadableGDALMap(
                mapUrl: TestUtils.testDataLocationFor(filename: "nrw_tiffs/E360640N5613980.tiff")
            )?.projectionReference)
    }
}

import Foundation
import XCTest

@testable import DeepSolarisKit

final class MapDirectoryTests: XCTestCase {
    func testMapDirectoryReturnsMapsInDirectory() {
        var mapDirectory = MapDirectory(
            at: TestUtils.testDataLocationFor(filename: "nrw_jp2_dataset"), fileExtension: "jp2")
        XCTAssertEqual(
            TestUtils.testDataLocationFor(
                filename: "nrw_jp2_dataset/dop10rgbi_32_280_5652_1_nw.jp2"),
            mapDirectory.next()?.mapUrl ?? nil)
        XCTAssertEqual(
            TestUtils.testDataLocationFor(
                filename: "nrw_jp2_dataset/dop10rgbi_32_294_5742_1_nw.jp2"),
            mapDirectory.next()?.mapUrl ?? nil)
        XCTAssertEqual(
            TestUtils.testDataLocationFor(
                filename: "nrw_jp2_dataset/dop10rgbi_32_296_5738_1_nw.jp2"),
            mapDirectory.next()?.mapUrl ?? nil)
        XCTAssertEqual(
            TestUtils.testDataLocationFor(
                filename: "nrw_jp2_dataset/dop10rgbi_32_299_5648_1_nw.jp2"),
            mapDirectory.next()?.mapUrl ?? nil)
        XCTAssertEqual(
            TestUtils.testDataLocationFor(
                filename: "nrw_jp2_dataset/dop10rgbi_32_316_5730_1_nw.jp2"),
            mapDirectory.next()?.mapUrl ?? nil)
    }
}

import Foundation
import XCTest

@testable import DeepSolarisKit

final class GDALTranslateMapTests: XCTestCase {

    func testMapExtentFromFilename() {
        XCTAssertEqual(
            Rect(topLeft: Point(x: 280000, y: 5_653_000), width: 1000, height: -1000),
            GDALTranslateMap(
                mapUrl: URL(fileURLWithPath: "file://dop10rgbi_32_280_5652_1_nw.jp2"),
                widthInMeters: 1000,
                heightInMeters: 1000
            )?.mapExtent ?? nil)

        XCTAssertEqual(
            Rect(topLeft: Point(x: 294000, y: 5_743_000), width: 1000, height: -1000),
            GDALTranslateMap(
                mapUrl: URL(fileURLWithPath: "file://dop10rgbi_32_294_5742_1_nw.jp2"),
                widthInMeters: 1000,
                heightInMeters: 1000
            )?.mapExtent ?? nil)

        XCTAssertEqual(
            Rect(topLeft: Point(x: 296000, y: 5_739_000), width: 1000, height: -1000),
            GDALTranslateMap(
                mapUrl: URL(fileURLWithPath: "file://dop10rgbi_32_296_5738_1_nw.jp2"),
                widthInMeters: 1000,
                heightInMeters: 1000
            )?.mapExtent ?? nil)

        XCTAssertEqual(
            Rect(topLeft: Point(x: 299000, y: 5_649_000), width: 1000, height: -1000),
            GDALTranslateMap(
                mapUrl: URL(fileURLWithPath: "file://dop10rgbi_32_299_5648_1_nw.jp2"),
                widthInMeters: 1000,
                heightInMeters: 1000
            )?.mapExtent ?? nil)

        XCTAssertEqual(
            Rect(topLeft: Point(x: 316000, y: 5_731_000), width: 1000, height: -1000),
            GDALTranslateMap(
                mapUrl: URL(fileURLWithPath: "file://dop10rgbi_32_316_5730_1_nw.jp2"),
                widthInMeters: 1000,
                heightInMeters: 1000
            )?.mapExtent ?? nil)

    }

    func testMapTransformFromFilename() {
        //translateX: worldVector[4], translateY: worldVector[5], scaleX: 0.10,
        //scaleY: -0.10, skewX: 0.0, skewY: 0.0)
        XCTAssertEqual(
            MapTransform(
                translateX: 280000, translateY: 5_653_000, scaleX: 0.10, scaleY: -0.10, skewX: 0.0,
                skewY: 0.0),
            GDALTranslateMap(
                mapUrl: URL(fileURLWithPath: "file://dop10rgbi_32_280_5652_1_nw.jp2"),
                widthInMeters: 1000,
                heightInMeters: 1000
            )?.mapTransform ?? nil)

        XCTAssertEqual(
            MapTransform(
                translateX: 294000, translateY: 5_743_000, scaleX: 0.10, scaleY: -0.10, skewX: 0.0,
                skewY: 0.0),
            GDALTranslateMap(
                mapUrl: URL(fileURLWithPath: "file://dop10rgbi_32_294_5742_1_nw.jp2"),
                widthInMeters: 1000,
                heightInMeters: 1000
            )?.mapTransform ?? nil)

        XCTAssertEqual(
            MapTransform(
                translateX: 296000, translateY: 5_739_000, scaleX: 0.10, scaleY: -0.10, skewX: 0.0,
                skewY: 0.0),
            GDALTranslateMap(
                mapUrl: URL(fileURLWithPath: "file://dop10rgbi_32_296_5738_1_nw.jp2"),
                widthInMeters: 1000,
                heightInMeters: 1000
            )?.mapTransform ?? nil)

        XCTAssertEqual(
            MapTransform(
                translateX: 299000, translateY: 5_649_000, scaleX: 0.10, scaleY: -0.10, skewX: 0.0,
                skewY: 0.0),
            GDALTranslateMap(
                mapUrl: URL(fileURLWithPath: "file://dop10rgbi_32_299_5648_1_nw.jp2"),
                widthInMeters: 1000,
                heightInMeters: 1000
            )?.mapTransform ?? nil)

        XCTAssertEqual(
            MapTransform(
                translateX: 316000, translateY: 5_731_000, scaleX: 0.10, scaleY: -0.10, skewX: 0.0,
                skewY: 0.0),
            GDALTranslateMap(
                mapUrl: URL(fileURLWithPath: "file://dop10rgbi_32_316_5730_1_nw.jp2"),
                widthInMeters: 1000,
                heightInMeters: 1000
            )?.mapTransform ?? nil)

    }

    func testPixelExtentDerivedFromFilename() {
        XCTAssertEqual(
            Rect(topLeft: Point(x: 0, y: 0), width: 10000, height: 10000),
            GDALTranslateMap(
                mapUrl: URL(fileURLWithPath: "file://dop10rgbi_32_280_5652_1_nw.jp2"),
                widthInMeters: 1000,
                heightInMeters: 1000
            )?.pixelExtent ?? nil)

        XCTAssertEqual(
            Rect(topLeft: Point(x: 0, y: 0), width: 10000, height: 10000),
            GDALTranslateMap(
                mapUrl: URL(fileURLWithPath: "file://dop10rgbi_32_294_5742_1_nw.jp2"),
                widthInMeters: 1000,
                heightInMeters: 1000
            )?.pixelExtent ?? nil)

        XCTAssertEqual(
            Rect(topLeft: Point(x: 0, y: 0), width: 10000, height: 10000),
            GDALTranslateMap(
                mapUrl: URL(fileURLWithPath: "file://dop10rgbi_32_296_5738_1_nw.jp2"),
                widthInMeters: 1000,
                heightInMeters: 1000
            )?.pixelExtent ?? nil)

        XCTAssertEqual(
            Rect(topLeft: Point(x: 0, y: 0), width: 10000, height: 10000),
            GDALTranslateMap(
                mapUrl: URL(fileURLWithPath: "file://dop10rgbi_32_299_5648_1_nw.jp2"),
                widthInMeters: 1000,
                heightInMeters: 1000
            )?.pixelExtent ?? nil)

        XCTAssertEqual(
            Rect(topLeft: Point(x: 0, y: 0), width: 10000, height: 10000),
            GDALTranslateMap(
                mapUrl: URL(fileURLWithPath: "file://dop10rgbi_32_316_5730_1_nw.jp2"),
                widthInMeters: 1000,
                heightInMeters: 1000
            )?.pixelExtent ?? nil)
    }

}

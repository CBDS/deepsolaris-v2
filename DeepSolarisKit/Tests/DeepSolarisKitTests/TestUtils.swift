import Foundation

public struct TestUtils {
  public static func testDataLocationFor(filename: String) -> URL {
    let currentPathUrl = URL(
      fileURLWithPath: FileManager.default.currentDirectoryPath, isDirectory: true)
    let testDataUrl = currentPathUrl.appendingPathComponent("TestData", isDirectory: true)
    return testDataUrl.appendingPathComponent(filename)
  }
}

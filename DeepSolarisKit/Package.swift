// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "DeepSolarisKit",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "DeepSolarisKit",
            targets: ["DeepSolarisKit"])
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        .package(name: "GEOSwift", url: "https://github.com/GEOSwift/GEOSwift.git", from: "7.2.0"),
        .package(url: "https://github.com/dehesa/CodableCSV.git", from: "0.6.4"),
        .package(
            name: "Progress", url: "https://github.com/jkandzi/Progress.swift.git", from: "0.4.0"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .systemLibrary(
            name: "CGDAL",
            path: "./CGDAL",
            pkgConfig: "gdal",
            providers: [
                .brew(["libgdal-dev"]),
                .apt(["libgdal-dev"]),
            ]
        ),
        .target(
            name: "DeepSolarisKit",
            dependencies: [
                "CGDAL", "GEOSwift", "CodableCSV", "Progress",
            ]),
        .testTarget(
            name: "DeepSolarisKitTests",
            dependencies: ["DeepSolarisKit"]),
    ]
)

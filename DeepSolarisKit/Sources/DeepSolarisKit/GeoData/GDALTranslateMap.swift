import Foundation

public struct GDALTranslateMap: RasterMap {
    let mapUrl: URL
    let widthInMeters: Double
    let heightInMeters: Double
    public var numberOfBands: Int = 4
    public var mapTransform: MapTransform
    public var mapExtent: Rect?
    public var projectionReference: String = ""

    public init?(mapUrl: URL, widthInMeters: Double, heightInMeters: Double) {
        self.mapUrl = mapUrl
        self.widthInMeters = widthInMeters
        self.heightInMeters = heightInMeters

        guard let coords = getCoordinatesFromFilename(mapUrl: mapUrl) else {
            return nil
        }

        self.mapExtent = Rect(
            topLeft: Point(x: coords.x, y: coords.y + heightInMeters), width: widthInMeters,
            height: -heightInMeters)
        self.mapTransform = MapTransform(worldVector: [
            0.1, 0.0, 0.0, -0.1, coords.x, coords.y + heightInMeters,
        ])
    }

    func getGdalCommand(
        from sourceFilename: String, to destinationFilename: String, boundingBox: Rect,
        outputFileType: String = "GTiff"
    ) -> [String] {
        return [
            "-of", "\(outputFileType)", "-co", "COMPRESS=JPEG", "-srcwin",
            "\(boundingBox.topLeft.x)",
            "\(boundingBox.topLeft.y)",
            "\(boundingBox.width)", "\(boundingBox.height)",
            "-colorinterp_4", "undefined",
            "\(sourceFilename)", "\(destinationFilename)",
        ]
    }

    func executeGdalCommand(
        from sourceFilename: String, to destinationFilename: String, boundingBox: Rect
    ) throws {
        let task = Process()
        task.executableURL = URL(fileURLWithPath: "/usr/bin/gdal_translate")
        let arguments = getGdalCommand(
            from: sourceFilename, to: destinationFilename, boundingBox: boundingBox)
        print("\(arguments)")
        task.arguments = arguments
        try task.run()
        task.waitUntilExit()
    }

    func writeTile(
        from sourceFilename: String, to destinationFilename: String,
        tileBoundingBox: Rect
    ) {
        do {
            try executeGdalCommand(
                from: sourceFilename, to: destinationFilename, boundingBox: tileBoundingBox)
        } catch {

        }
    }

    public func readData(pixelRect: Rect) -> Data? {
        return nil
    }

    public func writeData(data: Data, pixelRect: Rect, mapRect: Rect, to filename: URL) {
        writeTile(
            from: self.mapUrl.path, to: filename.path, tileBoundingBox: pixelRect)
    }
}

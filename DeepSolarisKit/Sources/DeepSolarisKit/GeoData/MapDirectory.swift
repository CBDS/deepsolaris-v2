import Foundation

struct MapDirectory: RasterMapCollection {

    typealias Element = GDALTranslateMap
    let directory: URL
    let mapFiles: [URL]
    var currentIndex = 0

    init(at directory: URL, fileExtension: String) {
        self.directory = directory
        self.mapFiles =
            filterFilesInDirectory(directory: directory, fileExtension: fileExtension)?.sorted()
            ?? []
    }

    mutating func next() -> GDALTranslateMap? {
        if currentIndex >= self.mapFiles.count {
            return nil
        }

        let mapUrl = self.mapFiles[currentIndex]

        currentIndex += 1
        return GDALTranslateMap(mapUrl: mapUrl, widthInMeters: 1000, heightInMeters: 1000)
    }
}

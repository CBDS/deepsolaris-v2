import CGDAL
import Foundation

extension Collection where Element == String {
    var cslConstList: CSLConstList? {
        self.buffer.unsafeCopy().baseAddress
    }
}

public class GDALMap: RasterMap {
    var gdalDataset: GDALDatasetH?
    public var mapTransform: MapTransform
    public var mapExtent: Rect?
    public var projectionReference: String = ""
    public var numberOfBands: Int {
        0
    }

    public init?(mapUrl: URL) {
        if GDALGetDriverCount() == 0 {
            GDALAllRegister()
        }
        self.mapTransform = MapTransform()
    }

    deinit {
        GDALClose(gdalDataset)
    }
}

public class ReadableGDALMap: GDALMap, ReadableRasterMap {
    public override init?(mapUrl: URL) {
        super.init(mapUrl: mapUrl)
        guard let dataset = GDALOpenEx(mapUrl.path, UInt32(GDAL_OF_RASTER | GDAL_OF_READONLY | GDAL_OF_VERBOSE_ERROR), nil, nil, nil) else {
            if let error = CPLGetLastErrorMsg() 
            {
                print("error: \(String(cString:error))")
            }
            return nil
        }
        self.gdalDataset = dataset

        //TODO: see if we can clean this up a bit
        let geoTransform = UnsafeMutablePointer<Double>.allocate(capacity: 6)
        defer {
            geoTransform.deallocate()
        }
        GDALGetGeoTransform(self.gdalDataset, geoTransform)
        self.mapTransform = MapTransform(gdalVector: geoTransform.toArray(capacity: 6))

        guard let mapPixelExtent = self.pixelExtent else {
            return nil
        }
        self.mapExtent = self.mapTransform * mapPixelExtent

    }

    public override var numberOfBands: Int {
        return Int(GDALGetRasterCount(gdalDataset))
    }

    public override var projectionReference: String {
        get {
            guard let projection = GDALGetProjectionRef(self.gdalDataset) else {
                return ""
            }
            return String(validatingUTF8: projection) ?? ""
        }
        set {}
    }

    public var pixelExtent: Rect? {
        guard let gdalRasterBand = GDALGetRasterBand(gdalDataset, 1) else {
            return nil
        }
        let width = Double(GDALGetRasterBandXSize(gdalRasterBand))
        let height = Double(GDALGetRasterBandYSize(gdalRasterBand))
        return Rect(topLeft: Point(x: 0.0, y: 0.0), width: width, height: height)
    }

    public func readData(pixelRect: Rect) -> Data? {
        let width = Int32(pixelRect.height)
        let height = Int32(pixelRect.width)
        let rasterCount = Int32(self.numberOfBands)
        let numberOfBytes = width * height * rasterCount
        let byteAlignment = MemoryLayout<UInt8>.alignment

        let rasterBandData = UnsafeMutableRawPointer.allocate(
            byteCount: Int(numberOfBytes), alignment: byteAlignment)
        defer {
            rasterBandData.deallocate()
        }

        let error = GDALDatasetRasterIO(
            gdalDataset,
            GF_Read,
            Int32(pixelRect.topLeft.x), Int32(pixelRect.topLeft.y),
            width, height,
            rasterBandData,
            width, height,
            GDT_Byte,
            rasterCount,
            nil,
            rasterCount, width * rasterCount, Int32(1))

        if error != CE_None {
            return nil
        }
        return Data(bytes: rasterBandData, count: Int(numberOfBytes))
    }
}

public class WritableGDALMap: GDALMap, WritableRasterMap {
    let gdalDriver: GDALDriverH
    var bandsNumber: Int = 4

    public override init?(mapUrl: URL) {
        guard let driver = GDALGetDriverByName("GTiff") else {
            return nil
        }
        self.gdalDriver = driver
        super.init(mapUrl: mapUrl)
    }

    public override var numberOfBands: Int {
        get { bandsNumber }
        set { bandsNumber = newValue }
    }

    public func writeData(
        data: Data, pixelRect: Rect, mapRect: Rect, to filename: URL,
        projectionReference: String
    ) {
        let width = Int32(pixelRect.height)
        let height = Int32(pixelRect.width)
        let rasterCount = Int32(self.numberOfBands)

        let options = ["COMPRESS=JPEG"]
        let writeDataset = withArrayOfCStrings(options) {
            cOptionArray in
            return GDALCreate(
                self.gdalDriver,
                filename.path, width, height, rasterCount,
                GDT_Byte, cOptionArray)
        }

        defer {
            GDALClose(writeDataset)
        }

        if let data = projectionReference.data(using: .utf8) {
            data.withUnsafeBytes { ptr in
                guard let bytes = ptr.baseAddress?.assumingMemoryBound(to: Int8.self) else {
                    return
                }
                GDALSetProjection(writeDataset, bytes)
            }
        }

        let geoTransform = MapTransform(mapExtent: mapRect, pixelExtent: pixelRect)
        var gdalTransform = geoTransform.gdalVector
        let _ = gdalTransform.withUnsafeMutableBufferPointer { buffer in
            GDALSetGeoTransform(writeDataset, buffer.baseAddress)
        }

        data.withUnsafeBytes { bandBufferData in
            let bandMutableBufferData = UnsafeMutableRawBufferPointer(mutating: bandBufferData)
            if let rasterBandData = bandMutableBufferData.baseAddress {
                let _ = GDALDatasetRasterIO(
                    writeDataset,
                    GF_Write,
                    Int32(0), Int32(0),
                    width, height,
                    rasterBandData,
                    width, height,
                    GDT_Byte,
                    rasterCount,
                    nil,
                    rasterCount, width * rasterCount, Int32(1))

                // Set fourth band interpretation as undefined as otherwise it will be treated as alpha.
                let fourthBand = GDALGetRasterBand(writeDataset, 4)
                GDALSetRasterColorInterpretation(fourthBand, GCI_Undefined)
                GDALFlushCache(writeDataset)
            }
        }
    }
}

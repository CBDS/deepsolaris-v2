import Foundation

public protocol RasterMap {
    var mapTransform: MapTransform { get }
    var mapExtent: Rect? { get }
    var numberOfBands: Int { get }
    var projectionReference: String { get set }
}

public protocol ReadableRasterMap: RasterMap {
    func readData(pixelRect: Rect) -> Data?
}

public protocol WritableRasterMap: RasterMap {
    var numberOfBands: Int { get set }

    func writeData(
        data: Data, pixelRect: Rect, mapRect: Rect, to filename: URL, projectionReference: String)
}

extension RasterMap {
    public var pixelExtent: Rect? {
        guard let inverse = mapTransform.inverse, let mapRect = self.mapExtent else {
            return nil
        }
        var pixelExtent = inverse * mapRect
        pixelExtent?.topLeft.x.round()
        pixelExtent?.topLeft.y.round()
        return pixelExtent
    }
}

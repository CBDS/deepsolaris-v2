import Foundation
import GEOSwift

public struct GeoData {
    init?(contentsOf filename: URL) {
        let decoder = JSONDecoder()
        guard let data = try? Data(contentsOf: filename) else {
            return nil
        }
        let geoJson = try? decoder.decode(GeoJSON.self, from: data)
    }

    var features: [Feature] {
        return []
    }

}

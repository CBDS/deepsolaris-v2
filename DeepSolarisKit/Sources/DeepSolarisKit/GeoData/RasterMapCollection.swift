import Foundation

protocol RasterMapCollection: Sequence, IteratorProtocol where Element: RasterMap {

}

public protocol TileIterator: Sequence, IteratorProtocol where Element == Rect {
    var numberOfTiles: Int { get }
}

extension Grid: TileIterator {
    public var numberOfTiles: Int {
        return self.numberOfCells
    }
}

import Foundation

public struct MapReader: TileReader {
    let map: ReadableRasterMap

    public init(map: ReadableRasterMap) {
        self.map = map
    }

    public func read(rect: Rect) -> Tile? {
        guard let mapExtent = self.map.mapTransform * rect,
            let readData = self.map.readData(pixelRect: rect)
        else {
            return nil
        }
        return Tile(
            data: readData,
            pixelExtent: rect, mapExtent: mapExtent,
            uniqueId: "E\(Int(mapExtent.bottomLeft.x))N\(Int(mapExtent.bottomLeft.y))",
            projectionReference: self.map.projectionReference)
    }
}

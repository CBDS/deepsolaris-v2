import Foundation

public struct MapWriter: TileWriter {
    let destinationUrl: URL
    let fileExtension: String
    let mapFactory: () -> WritableRasterMap

    public init(
        destinationUrl: URL, 
        fileExtension: String, 
        mapFactory: @escaping () -> WritableRasterMap
    ) 
    {
        self.destinationUrl = destinationUrl
        self.fileExtension = fileExtension
        self.mapFactory = mapFactory
    }

    public func write(tile: Tile) 
    {
        let rasterMap = mapFactory()
        let destinationFilename = self.destinationUrl.appendingPathComponent(tile.uniqueId)
            .appendingPathExtension(self.fileExtension)
        rasterMap.writeData(
            data: tile.data, pixelRect: tile.pixelExtent,
            mapRect: tile.mapExtent, to: destinationFilename,
            projectionReference: tile.projectionReference
        )
    }
}

import Foundation

public struct Tile {
    var data: Data = Data()
    var pixelExtent: Rect
    var mapExtent: Rect
    var uniqueId: String
    var projectionReference: String = ""
}

public protocol TileWriter {
    func write(tile: Tile)
}

public protocol TileReader {
    func read(rect: Rect) -> Tile?
}

public protocol TileProcessor {
    func process(tile: Tile) -> Tile?
}

typealias TileReaderWriter = TileReader & TileWriter




@resultBuilder
struct PipelineBuilder 
{
    static func buildBlock(_ steps: TileProcessor...) -> [TileProcessor]
    {
        return steps
    }
    
    static func buildEither(first component: [TileProcessor]) -> [TileProcessor] 
    {
        return component
    }

    static func buildEither(second component: [TileProcessor]) -> [TileProcessor] 
    {
        return component
    }

    static func buildArray(_ components: [[TileProcessor]]) -> [TileProcessor] {
        return components.flatMap{ $0 }
    }
}

enum PipelineProgressEvent 
{
    case pipelineStarted

    case pipelineReadTile(Tile)
    case pipelineTileProcessed(TileProcessor, Tile)
    case pipelineWroteTile(Tile)

    case pipelineEnded
}

protocol PipelineProgressCallback: Actor
{
    func progress(_ event: PipelineProgressEvent)
}


struct Pipeline<Iterator: TileIterator> 
{
    let source: TileReader
    let steps: [TileProcessor]
    let sink: TileWriter?
    let tileIterator: Iterator
    let callback: PipelineProgressCallback?

    init(source: TileReader, 
        sink: TileWriter? = nil, 
        tileIterator: Iterator, 
        callback: PipelineProgressCallback? = nil,
        @PipelineBuilder builder: () -> [TileProcessor]) 
    {
        self.source = source
        self.steps = builder()
        self.sink = sink
        self.tileIterator = tileIterator
        self.callback = callback
    }

    func run() async   
    {       
        await callback?.progress(.pipelineStarted) 
        for rect in tileIterator 
        {
            guard let readTile = source.read(rect: rect) else {
                continue
            }
            await callback?.progress(.pipelineReadTile(readTile))

            var processedTile = readTile
            for step in self.steps
            {
                guard let tile = step.process(tile: processedTile) else {
                    continue
                }
                await callback?.progress(.pipelineTileProcessed(step, tile))
                processedTile = tile
            }
            sink?.write(tile: processedTile)
            await callback?.progress(.pipelineWroteTile(processedTile))
        }
        await callback?.progress(.pipelineEnded)
    }
    
}
import CGDAL
import Foundation
import Progress

public struct DirectoryTiler {
    let sourceDirectory: URL
    let fileExtension: String
    let destinationDirectory: URL
    let tileResolutionInMeters: Double
   
    public init(
        at sourceDirectory: URL, fileExtension: String, to destinationDirectory: URL,
        tileResolutionInMeters: Double
    ) {
        self.sourceDirectory = sourceDirectory
        self.fileExtension = fileExtension
        self.destinationDirectory = destinationDirectory
        self.tileResolutionInMeters = tileResolutionInMeters
    }

    actor ProgressCallback: PipelineProgressCallback
    {
        var numberOfTilesProcessed: Int = 0
        var progressBar: ProgressBar 
        
        init(totalNumberOfTiles: Int) 
        {
            self.progressBar = ProgressBar(count: totalNumberOfTiles)
        }

        func progress(_ event: PipelineProgressEvent) 
        {
            switch(event)
            {
                case .pipelineWroteTile(_):
                    numberOfTilesProcessed += 1
                    self.progressBar.setValue(numberOfTilesProcessed)
                default:
                    break   
            }
        }        
    }


    public func run() throws 
    {
        guard let mapFiles = filterFilesInDirectory(
                directory: self.sourceDirectory, 
                fileExtension: fileExtension)?.sorted()
        else 
        {
            throw MapTilerError.mapDoesNotExist
        }

        //TODO: this was callled several times when it was located in the GDALMap constructor
        // Calling it here makes sure it is called from the main thread and only once
        // However we should make this more generic.
        if GDALGetDriverCount() == 0 {
            GDALAllRegister()
        }

        let progressCallback = ProgressCallback(totalNumberOfTiles: mapFiles.count)

        DispatchQueue.concurrentPerform(iterations: mapFiles.count) { index in
            do 
            {
                let mapUrl =  mapFiles[index]
                guard let rasterMap = ReadableGDALMap(mapUrl: mapUrl) else {
                    throw MapTilerError.mapDoesNotExist
                }
                try tile(mapUrl: mapUrl, rasterMap: rasterMap, callback: progressCallback)
            } 
            catch 
            {

            }
        }
    }

    func tile(mapUrl: URL, rasterMap: ReadableRasterMap, callback: ProgressCallback) throws 
    {
        guard let gridIterator = GridIterator(
                map: rasterMap, 
                strideX: tileResolutionInMeters * 10,
                strideY: tileResolutionInMeters * 10)
        else {
            print("Grid error")
            throw MapTilerError.gridError
        }

        let tileReader = MapReader(map: rasterMap)
        let tileWriter = MapWriter(
            destinationUrl: self.destinationDirectory, 
            fileExtension: "tiff"
        ) 
        {
            return WritableGDALMap(mapUrl: URL(fileURLWithPath: "")) as! WritableRasterMap
        }
        let mapTiler = MapTiler(
            tileIterator: gridIterator, tileReader: tileReader, tileProcessor: tileWriter,
            progressSteps: 10)

        var progressBar = ProgressBar(
            count: gridIterator.numberOfTiles,
            configuration: [
                ProgressString(string: mapUrl.lastPathComponent), ProgressBarLine(),
                ProgressPercent(),
                ProgressTimeEstimates(),
            ])
        mapTiler.run { (currentTileIndex, numberOfTiles) in
            progressBar.setValue(currentTileIndex)
        }
    }
}

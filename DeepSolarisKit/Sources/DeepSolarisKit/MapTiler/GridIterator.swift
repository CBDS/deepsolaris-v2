import Foundation

public struct GridIterator: TileIterator {
    var map: RasterMap
    public var grid: Grid

    public init?(map: RasterMap, strideX: Double, strideY: Double) {
        self.map = map
        guard let pixelExtent = self.map.pixelExtent else {
            return nil
        }
        self.grid = Grid(rect: pixelExtent, strideX: strideX, strideY: strideY)
    }

    public var numberOfTiles: Int {
        return self.grid.numberOfTiles
    }

    public mutating func next() -> Rect? {
        return self.grid.next()
    }
}

public struct MapTiler<Iterator: TileIterator> {
    let tileIterator: Iterator
    let tileReader: TileReader
    let tileWriter: TileWriter
    let progressSteps: Int

    public init(
        tileIterator: Iterator, tileReader: TileReader, tileProcessor: TileWriter,
        progressSteps: Int = 1
    ) {
        self.tileIterator = tileIterator
        self.tileReader = tileReader
        self.tileWriter = tileProcessor
        self.progressSteps = progressSteps
    }

    public func run(progress: ((Int, Int) -> Void)? = nil) {
        for (tileIndex, tileRect) in tileIterator.enumerated() {
            if let tile = tileReader.read(rect: tileRect) {
                tileWriter.write(tile: tile)
                if let progressFunction = progress {
                    if ((tileIndex + 1) % self.progressSteps) == 0 {
                        progressFunction(tileIndex + 1, tileIterator.numberOfTiles)
                    }
                }
            }
        }
    }
}

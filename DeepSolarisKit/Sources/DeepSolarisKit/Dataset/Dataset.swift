public protocol Dataset {
    associatedtype Item

    var name: String { get }
    var items: [Item] { get }
}

public protocol LabeledDataset: Dataset {
    associatedtype Label

    var labels: [Label] { get }
}

public typealias ClassificationLabel = ExpressibleByIntegerLiteral

public protocol ClassificationDataset: LabeledDataset where Label: ClassificationLabel {

}

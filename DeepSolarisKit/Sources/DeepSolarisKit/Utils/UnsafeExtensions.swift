import Foundation

extension Collection {
    /// Creates an UnsafeMutableBufferPointer with enough space to hold the elements of self.
    public func unsafeCopy() -> UnsafeMutableBufferPointer<Self.Element> {
        let copy = UnsafeMutableBufferPointer<Self.Element>.allocate(
            capacity: self.underestimatedCount)
        _ = copy.initialize(from: self)
        return copy
    }
}

extension Collection where Element == String {
    var buffer: UnsafeMutableBufferPointer<UnsafeMutablePointer<CChar>?> {
        self.map({ UnsafeMutablePointer($0.unsafeUTF8Copy().baseAddress) }).unsafeCopy()
    }
}

extension String {
    /// Create UnsafeMutableBufferPointer holding a null-terminated UTF8 copy of the string
    public func unsafeUTF8Copy() -> UnsafeMutableBufferPointer<CChar> {
        self.utf8CString.unsafeCopy()
    }
}

extension UnsafeMutablePointer {
    func toArray(capacity: Int) -> [Pointee] {
        return Array(UnsafeBufferPointer(start: self, count: capacity))
    }
}

public func withArrayOfCStrings<R>(
    _ args: [String],
    _ body: (UnsafeMutablePointer<UnsafeMutablePointer<CChar>?>) -> R
) -> R {
    var cStrings = args.map { strdup($0) }
    cStrings.append(nil)
    defer {
        cStrings.forEach { free($0) }
    }
    return cStrings.withUnsafeMutableBufferPointer { mutableBuffer in
        return body(mutableBuffer.baseAddress!)
    }
}

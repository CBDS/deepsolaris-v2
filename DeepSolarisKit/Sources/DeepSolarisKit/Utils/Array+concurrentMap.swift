import Foundation

extension Array {

    public func concurrentMap<T>(_ transform: (Element) -> T) -> [T] {
        var results = [Int: T]()

        let queue = DispatchQueue(
            label: Bundle.main.bundleIdentifier! + ".sync", attributes: .concurrent)

        DispatchQueue.concurrentPerform(iterations: count) { index in
            let result = transform(self[index])
            queue.async { results[index] = result }
        }

        return queue.sync(flags: .barrier) {
            (0..<results.count).map { results[$0]! }
        }
    }

}

import Foundation

func splitFilename(filename: String) -> (
    prefix: String, x: Int, y: Int, suffix: String
)? {
    let filenameComponents = filename.split(separator: "_")
    guard let x = Int(filenameComponents[2]), let bottomY = Int(filenameComponents[3]) else {
        return nil
    }
    let prefixString = filenameComponents[0...1].joined(separator: "_")
    let suffixString = filenameComponents[4...].joined(separator: "_")
    return (prefix: prefixString, x: x, y: bottomY, suffix: suffixString)
}

func getDestinationFilename(
    sourceFilename: String, boundingRect: Rect, fileExtension: String = "tiff"
) -> String? {
    guard let filenameComponents = splitFilename(filename: sourceFilename) else {
        return nil
    }
    return
        filenameComponents.prefix
        + "_\(Int(boundingRect.bottomLeft.x))_\(Int(boundingRect.bottomLeft.y))_"
        + filenameComponents.suffix.replacingOccurrences(of: "jp2", with: fileExtension)
}

func filterFilesInDirectory(directory: URL, fileExtension: String) -> [URL]? {
    do {
        let directoryContents = try FileManager.default.contentsOfDirectory(
            at: directory, includingPropertiesForKeys: nil)
        return directoryContents.filter { $0.absoluteString.contains(fileExtension) }
    } catch {

        print("Error")
    }
    return nil
}

func getCoordinatesFromFilename(mapUrl: URL) -> (x: Double, y: Double)? {
    guard let filenameComponents = splitFilename(filename: mapUrl.lastPathComponent) else {
        return nil
    }
    let x = Double(filenameComponents.x) * 1000
    let y =
        Double(filenameComponents.y)
        * 1000
    return (x: x, y: y)
}

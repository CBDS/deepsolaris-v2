enum MapTilerError: Error {
    case mapDoesNotExist
    case gridError
}

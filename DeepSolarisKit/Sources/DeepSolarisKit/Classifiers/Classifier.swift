public protocol Classifier {
  func fit<D: ClassificationDataset>(dataset: D)
  func predict<D: ClassificationDataset>(dataset: D)

}

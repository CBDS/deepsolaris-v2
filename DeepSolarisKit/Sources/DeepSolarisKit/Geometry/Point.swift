import Foundation

public struct Point: Hashable {
    var x: Double
    var y: Double
    var z: Double?

}

extension Point: Equatable {
    public static func == (lhs: Point, rhs: Point) -> Bool {
        return lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z
    }
}

extension Point {
    init(vector2: SIMD2<Double>) {
        self.init(x: vector2.x, y: vector2.y)
    }

    init(vector3: SIMD3<Double>) {
        self.init(x: vector3.x, y: vector3.y, z: vector3.z)
    }

    var vector2: SIMD2<Double> {
        return SIMD2(x, y)
    }

    var vector3: SIMD3<Double> {
        return SIMD3(self.vector2, z ?? 1.0)
    }
}

extension Point {
    static func + (lhs: Point, rhs: Point) -> Point {
        var z: Double?
        if let lz = lhs.z, let rz = rhs.z {
            z = lz + rz
        }
        return Point(x: lhs.x + rhs.x, y: lhs.y + rhs.y, z: z)
    }
}

import Foundation

public struct Grid: Sequence, IteratorProtocol {
    public typealias Element = Rect

    var current = 0
    var boundingRect: Rect
    let strideX: Double
    let strideY: Double

    public init(rect: Rect, strideX: Double, strideY: Double) {
        self.boundingRect = rect
        self.strideX = strideX
        self.strideY = strideY
    }

    public init(
        startX: Double = 0.0, startY: Double = 0.0, width: Double, height: Double, strideX: Double,
        strideY: Double
    ) {
        self.init(
            rect:
                Rect(topLeft: Point(x: startX, y: startY), width: width, height: height),
            strideX: strideX,
            strideY: strideY)
    }

    public var numberOfRows: Int {
        Int(ceil(self.boundingRect.height / self.strideY))
    }

    public var numberOfColumns: Int {
        Int(ceil(self.boundingRect.width / self.strideX))
    }

    public var numberOfCells: Int {
        return numberOfRows * numberOfColumns
    }

    public mutating func next() -> Rect? {
        if current >= numberOfCells {
            return nil
        }
        let x = boundingRect.topLeft.x + (Double(current % self.numberOfColumns) * self.strideX)
        let y = boundingRect.topLeft.y + (Double(current / self.numberOfColumns) * self.strideY)

        let rightX = x + self.strideX
        let cellWidth =
            rightX <= self.boundingRect.bottomRight.x
            ? self.strideX : self.boundingRect.bottomRight.x - x

        let bottomY = y + self.strideY
        let cellHeight =
            bottomY <= self.boundingRect.bottomRight.y
            ? self.strideY : self.boundingRect.bottomRight.y - y
        let rect = Rect(
            topLeft: Point(x: x, y: y), width: cellWidth, height: cellHeight)
        current += 1
        return rect
    }

}

public struct Rect: Hashable {
    var topLeft: Point
    var width: Double
    var height: Double

    init(topLeft: Point, width: Double, height: Double) {
        self.topLeft = topLeft
        self.width = width
        self.height = height
    }

    init(topLeft: Point, bottomRight: Point) {
        /*precondition(
            topLeft.x <= bottomRight.x && topLeft.y <= bottomRight.y,
            "Bottom right coordinate values should be equal to or larger than the topLeft")*/
        self.init(
            topLeft: topLeft, width: bottomRight.x - topLeft.x, height: bottomRight.y - topLeft.y)
    }

    var bottomRight: Point {
        Point(x: topLeft.x + width, y: topLeft.y + height)
    }

    var bottomLeft: Point {
        Point(x: topLeft.x, y: topLeft.y + height)
    }

    func move(deltaX: Double, deltaY: Double) -> Rect
    {
        let newTopLeft = Point(x: topLeft.x + deltaX, y: topLeft.y + deltaY)
        return Rect(topLeft: newTopLeft, width: self.width, height: self.height)
    }   
}

extension Rect: Equatable {
    public static func == (lhs: Rect, rhs: Rect) -> Bool {
        return lhs.topLeft == rhs.topLeft && lhs.width == rhs.width && lhs.height == rhs.height
    }

}

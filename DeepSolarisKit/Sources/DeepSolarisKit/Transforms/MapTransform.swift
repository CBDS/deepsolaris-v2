import Foundation

public struct MapTransform: Equatable {
    var transformX: SIMD3<Double>
    var transformY: SIMD3<Double>

    init(
        translateX: Double = 0.0, translateY: Double = 0.0, scaleX: Double = 1.0,
        scaleY: Double = 1.0, skewX: Double = 0.0, skewY: Double = 0.0
    ) {
        self.transformX = SIMD3(scaleX, skewX, translateX)
        self.transformY = SIMD3(skewY, scaleY, translateY)
    }

    init(mapExtent: Rect, pixelExtent: Rect) {
        self.transformX = SIMD3(mapExtent.width / pixelExtent.width, 0.0, mapExtent.topLeft.x)
        self.transformY = SIMD3(0.0, mapExtent.height / pixelExtent.height, mapExtent.topLeft.y)
    }

    init(worldVector: [Double]) {
        self.init(
            translateX: worldVector[4], translateY: worldVector[5], scaleX: worldVector[0],
            scaleY: worldVector[3], skewX: worldVector[2], skewY: worldVector[1])
    }

    init(gdalVector: [Double]) {
        self.init(
            translateX: gdalVector[0], translateY: gdalVector[3], scaleX: gdalVector[1],
            scaleY: gdalVector[5], skewX: gdalVector[2], skewY: gdalVector[4])
    }

    var determinant: Double {
        return (self.transformX.x * self.transformY.y) - (self.transformX.y * self.transformY.x)
    }

    var isDegenerate: Bool {
        return self.determinant == 0
    }

    var inverse: MapTransform? {
        // Determine inverse via this algorithm:
        // https://aaronschlegel.me/calculate-matrix-inverse-2x2-3x3.html
        // [[a b c]
        //  [d e f]
        //  [g h i]
        // ]
        // Inverse:
        // [[(ei - fh) -(bi - ch) (bf - ce)]
        // [(-di - fg) (ai - cg) -(ad - cd)]
        // [(dh - eg) -(ah - bg) (ae - bd)]]
        //
        //  [g h i] = always [0 0 1]
        // so:
        // [ [e -b (b * f - c * e)]
        // [ -d a  -(a * d - c * d)]
        // [ 0 0 (a * e - b * d)]]
        // Then: a * e - b * d = 1?
        if isDegenerate {
            return nil
        }
        let iDet = 1.0 / determinant
        let ra = self.transformY.y * iDet
        let rb = -1.0 * self.transformX.y * iDet
        let translateX =
            iDet
            * (self.transformX.y * self.transformY.z - self.transformX.z * self.transformY.y)
        let rd = -1.0 * self.transformY.x * iDet
        let re = self.transformX.x * iDet
        let translateY =
            -1.0 * iDet
            * (self.transformX.x * self.transformY.z - self.transformX.z * self.transformY.x)
        return MapTransform(
            translateX: translateX, translateY: translateY, scaleX: ra, scaleY: re, skewX: rb,
            skewY: rd)
    }

    static func * (lhs: MapTransform, rhs: SIMD2<Double>) -> SIMD2<Double> {
        let coordinate = SIMD3(rhs, 1)
        let x = (lhs.transformX * coordinate).sum()
        let y = (lhs.transformY * coordinate).sum()
        return SIMD2(x, y)
    }

}

extension MapTransform {
    static func * (lhs: MapTransform, rhs: Point) -> Point {
        let transformedCoordinate = lhs * rhs.vector2
        return Point(vector2: transformedCoordinate)
    }

    public static func == (lhs: MapTransform, rhs: MapTransform) -> Bool {
        return lhs.transformX == rhs.transformX && lhs.transformY == rhs.transformY
    }

}

extension MapTransform {
    // TODO this should actually be a Polygon, a Rect under a affine transform
    // can be distorted into a polygon.
    static func * (lhs: MapTransform, rhs: Rect) -> Rect? {
        let transformedTopLeft = lhs * rhs.topLeft
        let transformedBottomRight = lhs * rhs.bottomRight
        return Rect(topLeft: transformedTopLeft, bottomRight: transformedBottomRight)
    }
}

extension MapTransform {
    var gdalVector: [Double] {
        [
            self.transformX.z, self.transformX.x, self.transformX.y, self.transformY.z,
            self.transformY.x, self.transformY.y,
        ]
    }
}

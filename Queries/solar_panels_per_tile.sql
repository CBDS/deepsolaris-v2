create view register_tile.selected_buildings
as
select
    pa.*
from register_tile.corop_zl_2020 as co
inner join bagactueel.pandactueelbestaand as pa on ST_Intersects(co.geom, pa.geovlak);

select * from register_tile.selected_buildings;

drop table if exists register_tile.buildings_grid_cell;
create table register_tile.buildings_grid_cell as
select
	ST_Intersection(gz.geom, sb.geovlak) as building_part_geom,
	ST_Area(ST_Intersection(gz.geom, sb.geovlak)) / ST_Area(sb.geovlak) as building_area_fraction,
	gz.gid as cell_id,
	sb.gid as building_id
from register_tile.grid_zl_20m_selected as gz
inner join register_tile.selected_buildings as sb on ST_Intersects(gz.geom, sb.geovlak);

create index buildings_grid_cell_geom_index on register_tile.buildings_grid_cell using gist(building_part_geom);

alter table register_tile.buildings_grid_cell
add constraint fk_building_id
foreign key (building_id) references bagactueel.pand(gid);

alter table register_tile.buildings_grid_cell
add constraint fk_cell_id
foreign key (gid_id) references register_tile.grid_zl_20m_selected(gid);

drop table if exists  register_tile.buildings_grid_cell_labeled;
create table register_tile.buildings_grid_cell_labeled as
select 
	case when 
		pas.pv_building_id[1] is null then 0 
		else 1 
	end as solar_panel_label,
	case when 
		pas.pv_building_id is null or pas.pv_building_id[1] is null 
	then 0 
	else building_area_fraction
	end as weighted_solar_panel_label,
	bgc.building_part_geom,
	bgc.building_id,
	bgc.cell_id,
	pas.p_gid
from register_tile.buildings_grid_cell as bgc
left join register_tile.pand_adres_solar_panels as pas on bgc.building_id = pas.p_gid;

create index buildings_labeled_grid_cell_geom_index on register_tile.buildings_grid_cell_labeled using gist(building_part_geom);

select distinct(solar_panel_label)
from register_tile.buildings_grid_cell_labeled;

select *
from register_tile.buildings_grid_cell
limit 100;

drop table if exists register_tile.buildings_grid_cell_labeled_aggr;
create table register_tile.buildings_grid_cell_labeled_aggr as
select 
	cell_id,
	gzs.geom,
	case
		when count(distinct(building_id)) > 0 
	then sum(weighted_solar_panel_label) / count(distinct(building_id))
	else 0
	end as weighted_label,
	sum(ST_Area(gcl.building_part_geom)) as total_building_area,
	count(distinct(building_id)) as number_of_buildings,
	sum(weighted_solar_panel_label) as building_area_fraction_sum
from register_tile.buildings_grid_cell_labeled as gcl
inner join register_tile.grid_zl_20m_selected as gzs on gzs.gid = gcl.cell_id
group by cell_id, gzs.geom;

create index buildings_labeled_aggr_grid_cell_geom_index on register_tile.buildings_grid_cell_labeled_aggr using gist(geom);
		 
		 
drop table if exists register_tile.buildings_grid_cell_aggr;
create table register_tile.buildings_grid_cell_aggr as
select 
	gzs.gid,
	gzs.geom,	
	case 
		when sum(ST_Area(gcl.building_part_geom)) is not null
		then sum(ST_Area(gcl.building_part_geom))
		else 0 
	end as total_building_area,
	count(distinct(gcl.building_id)) as number_of_buildings 
from register_tile.grid_zl_20m_selected as gzs 
left join register_tile.buildings_grid_cell as gcl on gzs.gid = gcl.cell_id
group by gzs.gid, gzs.geom;

create index buildings_aggr_grid_cell_geom_index on register_tile.buildings_grid_cell_aggr using gist(geom);
-- alter table register_tile.buildings_grid_cell_labeled
-- add constraint fk_building_id
-- foreign key (building_id) references register_tile.buildings_grid_cell(building_id);

-- alter table register_tile.buildings_grid_cell_labeled
-- add constraint fk_pand_adres_solar_panels
-- foreign key (p_gid) references register_tile.pand_adres_solar_panels(p_gid);

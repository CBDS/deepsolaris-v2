-- 8917402
-- 6072139
select 
   count(distinct(pandid))
from bagactueel.adres_full as a
inner join bagactueel.pandactueelbestaand as p on p.identificatie = a.pandid;

-- 353570
select 
	count(distinct(identificatie))
from register_tile.selected_buildings;

-- 353570
select 
	count(distinct(building_id))
from register_tile.buildings_grid_cell as bgc;

-- left join/distinct: 353570
-- inner join/distinct: 234060
-- difference: 119510
select 
	count(distinct(building_id))
from register_tile.buildings_grid_cell as bgc
inner join register_tile.pand_adres_solar_panels as pas on bgc.building_id = pas.p_gid;

select
	count(distinct(pandid)),
	sum(number_of_objects)
from register_tile.buildings_grid_cell as bgc
inner join register_tile.pand_adres_solar_panels as pas on bgc.building_id = pas.p_gid
where number_of_objects > 1;

select 
   count(distinct(pandid)),
   sum(case when pandid is null then 1 else 0 end) as addresses_without_building
from bagactueel.pandactueelbestaand as p
LEFT join bagactueel.adres_full as a on p.identificatie = a.pandid;

--99534 pandids not in pandactueelbestaand
-- 35606 no pandid assigned = NULL
-- 135140 not joinable
select 
	 count(*)
from bagactueel.adres_full as a 
left join bagactueel.pandactueelbestaand as p on p.identificatie = a.pandid
where identificatie is null;

-- 9052542
-- 6121174 distinct pandids
select 
   count(distinct(pandid))
from bagactueel.adres_full as a;

-- 10006040
select 
 count(distinct(identificatie)) 
from bagactueel.pandactueelbestaand;

-- 6072139
select
	count(*)
from register_tile.pand_adres_solar_panels;
	
-- 35606
select 
	count(*)
from bagactueel.adres_full where pandid is null;
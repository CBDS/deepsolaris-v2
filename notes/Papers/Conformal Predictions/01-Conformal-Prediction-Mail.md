Hi guys,

If you're interested more in how conformal prediction works, here are 
the sources Christof sent to me. Most important sources:

  - Conformal Prediction in 2020 by Emmanuel Candes lecture: 
https://www.youtube.com/watch?v=61tpigfLHso

  - Conformalized Quantile Regression paper at NIPS 2019 - shows how if 
you combine quantile regression with conformal prediction you can obtain 
predictive intervals which account for hederoskedasticity. One question 
that remains is how well this is suited for image 
recognition/regression: 
https://papers.nips.cc/paper/8613-conformalized-quantile-regression

Extra sources to get a better feel/how to deal with domain shift:

  - How The Washington Post Estimates Outstanding Votes for the 2020 
Presidential Election - a nice writeup about one use case, where The 
Washington Post uses conformalized quantile regression to predict 
election results in the US. They don't base it on images, of course, so 
the use case is quite different from ours: (attached)

  - Conformal Prediction Under Covariate Shift paper at NIPS 2019 - 
talks about adapting the conformal prediction methods to obtain 
predictive intervals when testing on a domain with a different data 
distribution. There is a wobbly point here that, if the "likelihood 
ratio" function between the two domains is not known, it has to be 
estimated by e.g. random forests or logistic regression which adds a 
layer of non-reliability. They only check it on very simple data: 
http://papers.nips.cc/paper/8522-conformal-prediction-under-covariate-shift

  - Robust Validation: Confident Predictions Even When Distributions 
Shift - a thick paper that shows a methodology for dealing with 
predictive inference in spite of distributional shift, though I have to 
say I didn't quite get what they propose because it's a tad too thick 
https://arxiv.org/abs/2008.04267

Best regards,

Chris


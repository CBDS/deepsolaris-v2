# Experiments

## Image Data Augmentation

Evaluate effectiveness different augmentation methods on validation/testing performance. Figure out if the same augmentation applies for remote sensing images as natural images. Do the same Image Data Augmentation techniques work? 

Check out papers on data augmentation too:
- [The Effectiveness of Data Augmentation in Image Classification using Deep Learning](https://arxiv.org/abs/1712.04621)
- [Understanding data augmentation for classification: when to warp?](https://arxiv.org/pdf/1609.08764.pdf) 
- [Data augmentation instead of explicit regularization](https://arxiv.org/abs/1806.03852)
- [On Feature Normalization and Data Augmentation](https://arxiv.org/abs/2002.11102)
- [Regularizing Deep Networks with Semantic Data Augmentation](https://arxiv.org/abs/2007.10538)
- [Can We Achieve More with Less? Exploring Data Augmentation for Toxic Comment Classification](https://arxiv.org/abs/2007.00875)
- [E-Stitchup: Data Augmentation for Pre-Trained Embeddings](https://arxiv.org/pdf/1912.00772.pdf)
- [Data Augmentation for Meta-Learning](https://arxiv.org/abs/2010.07092)
- [Training Data Augmentation for Deep Learning RF Systems](https://arxiv.org/abs/2010.00178?context=eess.SP)
- [Data Augmentation by Pairing Samples for Images Classification](https://arxiv.org/pdf/1801.02929.pdf)
- [Image Augmentation Is All You Need: Regularizing Deep Reinforcement Learning from Pixels](https://arxiv.org/abs/2004.13649)
- [A Preliminary Study on Data Augmentation of Deep Learning for Image Classification](https://arxiv.org/abs/1906.11887)

## Evaluate different classification networks on DeepSolaris datasets

- VGG: VGG16
- ResNet: ResNet50
- Inception: InceptionV3

## Evaluate several segmentation networks on annotated solar panel polygons

## Compare segmentation CNNs with more traditional Computer Vision algorithms

## Evaluate the effect of sampling methods on training effectiveness

- Size of training set
- The composition of the training set; random samples vs. stratified samples
- Importance of background variety?

## Evaluate Annotation and Annotator quality

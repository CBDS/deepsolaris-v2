# Labelling on the basis of Register Data

- Weighted register label:
	- 0 when no solar panels
	- building area fraction in grid when 1 --> does this make sense?
- Added weighted register labels and normalize them between 0 and 1:
	- by dividing them throught the number of buildings?
	- weighed sum of building area??

## Problems

- Large buildings with solar panels. The buildings are spread across a lot of grid cells, so the building fraction and therefore weighted solar panel label is low. While this is correct in some cells that do not contain solar panels, it is incorrect for cells that contain all of the solar panels. For instance see the old CBS building for examples.

## Questions

- Do we need to weight the build areas by the fraction: area / grid cell area? 
- Do we need to take the total area of the building into account? Especially for larger buildings?
- Better weighing schemer??
	- Total area compared to grid cell? * label
	- divided by total area of building?
	 


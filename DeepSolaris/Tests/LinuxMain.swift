import XCTest

import DeepSolarisTests

var tests = [XCTestCaseEntry]()
tests += DeepSolarisTests.allTests()
XCTMain(tests)

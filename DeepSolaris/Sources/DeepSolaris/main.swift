import ArgumentParser
import DeepSolarisKit
import Foundation
import Progress

struct MapTilerApp: ParsableCommand {

    @Option(name: .shortAndLong, help: "The directory containing the source images")
    var imageDirectory: String = "/media/ImageData/DE/aerial images/HR/selections/Bonn_2019_HR_1km"

    @Option(name: .shortAndLong, help: "The destination directory")
    var destinationDirectory: String

    @Option(name: .shortAndLong, help: "The image width resolution in meters")
    var resolutionInMeters: Int = 1000

    @Option(name: .shortAndLong, help: "The destination tile resolution in meters")
    var tileResolutionInMeters: Int = 20

    @Option(name: .shortAndLong, help: "The file extension of the images")
    var fileExtension: String = ".jp2"

    func run() throws {
        let directoryTiler = DirectoryTiler(
            at: URL(fileURLWithPath: imageDirectory), fileExtension: fileExtension,
            to: URL(fileURLWithPath: destinationDirectory),
            tileResolutionInMeters: Double(tileResolutionInMeters))
        try directoryTiler.run()
    }

}

MapTilerApp.main()
